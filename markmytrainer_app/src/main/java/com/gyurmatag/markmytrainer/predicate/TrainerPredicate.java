package com.gyurmatag.markmytrainer.predicate;

import com.gyurmatag.markmytrainer.model.QTrainer;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;

import java.util.Optional;

public class TrainerPredicate {
    private TrainerPredicate() {
    }

    //TODO: később törölni

    public static Predicate advancedGymSearch(Optional<String> trainerName, Optional<String> gymName, Optional<String> cityName) {
        JPAQuery query = new JPAQuery();
        BooleanBuilder where = new BooleanBuilder();

        if (trainerName.isPresent()) {
            where.and(QTrainer.trainer.name.containsIgnoreCase(trainerName.get().trim()));
        }

        if (gymName.isPresent()) {
            where.and(QTrainer.trainer.gym.name.containsIgnoreCase(gymName.get().trim()));
        }

        if (cityName.isPresent()) {
            where.and(QTrainer.trainer.gym.city.name.containsIgnoreCase(cityName.get().trim()));
        }

        return where;

    }
}
