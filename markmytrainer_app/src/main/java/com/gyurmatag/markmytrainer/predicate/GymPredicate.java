package com.gyurmatag.markmytrainer.predicate;

import com.gyurmatag.markmytrainer.model.QGym;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;

import java.util.Optional;

public class GymPredicate {
    private GymPredicate() {
    }

    public static Predicate advancedGymSearch(Optional<String> gymName, Optional<String> cityName) {
        JPAQuery query = new JPAQuery();
        BooleanBuilder where = new BooleanBuilder();

        if (gymName.isPresent()) {
            where.and(QGym.gym.name.containsIgnoreCase(gymName.get().trim()));
        }

        if (cityName.isPresent()) {
            where.and(QGym.gym.city.name.containsIgnoreCase(cityName.get().trim()));
        }

        return where;

    }
}
