package com.gyurmatag.markmytrainer.config;

import com.gyurmatag.markmytrainer.security.MyCustomLoginSuccessHandler;
import com.gyurmatag.markmytrainer.service.UserService;
import com.gyurmatag.markmytrainer.web.FlashMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserService userService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private MyCustomLoginSuccessHandler myCustomLoginSuccessHandler;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "/vendor/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .requiresChannel()
                .requestMatchers(r -> r.getHeader("X-Forwarded-Proto") != null)
                .requiresSecure()
                .and()
                .exceptionHandling().authenticationEntryPoint(new AuthenticationEntryPoint() {

            @Override
            public void commence(HttpServletRequest request, HttpServletResponse response,
                                 AuthenticationException authException) throws IOException {
                if (authException != null) {
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    request.getSession().setAttribute("loginFlash", new FlashMessage(messageSource.getMessage("unauthorized.access", null, Locale.US), FlashMessage.Status.DANGER));
                    response.sendRedirect("/login");
                }
            }
        })
                .and()
                .authorizeRequests()
                .antMatchers("/gyms/add", "/trainers/add", "/trainers/{[\\d]}/review", "/user/reviews", "/user/editCity", "/user/newPassword").authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .successHandler(myCustomLoginSuccessHandler)
                .failureHandler(loginFailureHandler())
                .and()
                .logout()
                .permitAll()
                .logoutSuccessHandler(logoutSuccessHandler())
                .and()
                .csrf();
    }


    public AuthenticationFailureHandler loginFailureHandler() {
        return (request, response, exception) -> {
            if (exception.getClass().isAssignableFrom(BadCredentialsException.class)) {
                request.getSession().setAttribute("loginFlash", new FlashMessage(messageSource.getMessage("login.failure", null, Locale.US), FlashMessage.Status.DANGER));
                response.sendRedirect("/login");
            } else {
                if (exception.getClass().isAssignableFrom(DisabledException.class)) {
                    request.getSession().setAttribute("loginFlash", new FlashMessage(messageSource.getMessage("login.failure.disabled", null, Locale.US), FlashMessage.Status.DANGER));
                    response.sendRedirect("/login");
                }
            }
        };
    }

    public LogoutSuccessHandler logoutSuccessHandler() {
        return (request, response, exception) -> {
            response.sendRedirect("/login");
        };
    }

}

