package com.gyurmatag.markmytrainer.web.error;

public class TrainerNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 5861310537366287163L;

    public TrainerNotFoundException() {
        super();
    }

    public TrainerNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public TrainerNotFoundException(final String message) {
        super(message);
    }

    public TrainerNotFoundException(final Throwable cause) {
        super(cause);
    }
}
