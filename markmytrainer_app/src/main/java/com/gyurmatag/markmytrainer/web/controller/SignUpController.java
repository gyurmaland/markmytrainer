package com.gyurmatag.markmytrainer.web.controller;

import com.gyurmatag.markmytrainer.model.City;
import com.gyurmatag.markmytrainer.model.User;
import com.gyurmatag.markmytrainer.service.CityService;
import com.gyurmatag.markmytrainer.service.MailClientService;
import com.gyurmatag.markmytrainer.service.RoleService;
import com.gyurmatag.markmytrainer.service.UserService;
import com.gyurmatag.markmytrainer.web.FlashMessage;
import com.gyurmatag.markmytrainer.web.dto.AutocompleteForAllCitiesDto;
import com.gyurmatag.markmytrainer.web.dto.SignUpDto;
import com.gyurmatag.markmytrainer.web.error.BadActivationCodeException;
import com.gyurmatag.markmytrainer.web.error.PasswordNoMatchException;
import com.gyurmatag.markmytrainer.web.error.UserEmailAlreadyExistException;
import com.gyurmatag.markmytrainer.web.error.UsernameAlreadyExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
public class SignUpController extends BaseController {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private CityService cityService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private MailClientService mailClientService;

    @Value("${spring.mail.activation.webappurl}")
    private String WEB_APP_URL_FOR_ACTIVATION;

    @ModelAttribute("signUpForm")
    public SignUpDto signUpDto() {
        return new SignUpDto();
    }


    // Form for the Sign Up
    @RequestMapping(path = "/signup", method = RequestMethod.GET)
    public String signUpForm(Model model, HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getPrincipal() instanceof UserDetails) {
            return "redirect:/trainers";
        }

        if (!model.containsAttribute("user")) {
            model.addAttribute("user", new User());
        }

        backTimes = calculateBackTimes(request, false, "signup");

        model.addAttribute("heading", "Sign Up");
        model.addAttribute("action", "/signup");
        model.addAttribute("backTimes", backTimes);

        return "authentication/signup";
    }

    // Register a new user
    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("signUpForm") @Valid SignUpDto signUpDto, BindingResult result, RedirectAttributes redirectAttributes) {
        //TODO: mapperek írása
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".signUpForm", result);

            redirectAttributes.addFlashAttribute("signUpForm", signUpDto);

            // Redirect back to the form
            return "redirect:/signup";
        }

        User user = cityService.addIfNotExistSetIfExistsAtUsers(signUpDto);

        //TODO: egymásra építeni ezeket az üziket mert csak egy jelenik meg amelyik éppen dobja az exceptiont
        try {
            userService.save(user);
            mailClientService.prepareAndSendVerificationEmail(user.getEmail(), "Regisztárció megerősítése", WEB_APP_URL_FOR_ACTIVATION + user.getActivation());
        } catch (UserEmailAlreadyExistException | UsernameAlreadyExistException | PasswordNoMatchException userex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage(userex.getMessage(), FlashMessage.Status.DANGER));
            redirectAttributes.addFlashAttribute("signUpForm", signUpDto);
            return "redirect:/signup";
        }

        // Add flash message for success
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Sign Up was successfull! Verification link sent out to this e-mail: " + user.getEmail(), FlashMessage.Status.SUCCESS));
        return "redirect:/login";
    }

    // Activate a user
    @RequestMapping(path = "/activation/{code}", method = RequestMethod.GET)
    public String activation(@PathVariable("code") String code, HttpServletResponse response, RedirectAttributes redirectAttributes) {
        String currentUsername = null;
        try {
            currentUsername = userService.findByActivation(code).getUsername();
            userService.userActivation(code);
        } catch (BadActivationCodeException bacex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage(bacex.getMessage(), FlashMessage.Status.DANGER));
            return "redirect:/signup";
        }

        redirectAttributes.addFlashAttribute("flash", new FlashMessage(messageSource.getMessage("user.activation.success", new Object[]{currentUsername}, Locale.US), FlashMessage.Status.SUCCESS));
        return "redirect:/login";
    }


    @ResponseBody
    @RequestMapping(value = "/autocompleteCitesAll", method = RequestMethod.GET)
    public Map<String, List<AutocompleteForAllCitiesDto>> autoCompleteCityList(@RequestParam("searchstr") String searchstr) {
        Map<String, List<AutocompleteForAllCitiesDto>> map = new HashMap<>();

        List<AutocompleteForAllCitiesDto> cityList = cityService.findTop5ByNameContainsIgnoreCase(searchstr.trim());

        map.put("cityList", cityList);
        return map;
    }
}

