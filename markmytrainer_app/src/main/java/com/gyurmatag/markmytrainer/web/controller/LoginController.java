package com.gyurmatag.markmytrainer.web.controller;

import com.gyurmatag.markmytrainer.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController {

    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String loginForm(Model model, HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getPrincipal() instanceof UserDetails) {
            return "redirect:/trainers";
        }

        model.addAttribute("user", new User());
        try {
            Object loginFlash = request.getSession().getAttribute("loginFlash");
            model.addAttribute("loginFlash", loginFlash);

            request.getSession().removeAttribute("loginFlash");
        } catch (Exception ex) {
            // "flash" session attribute must not exist...do nothing and proceed normally
        }


        model.addAttribute("heading", "Login");
        model.addAttribute("action", "/login");

        return "authentication/login";
    }

    @RequestMapping("/access_denied")
    public String accessDenied() {
        return "access_denied";
    }
}
