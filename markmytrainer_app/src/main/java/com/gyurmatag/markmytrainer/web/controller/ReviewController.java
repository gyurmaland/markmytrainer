package com.gyurmatag.markmytrainer.web.controller;

import com.gyurmatag.markmytrainer.model.Pager;
import com.gyurmatag.markmytrainer.model.Review;
import com.gyurmatag.markmytrainer.model.Trainer;
import com.gyurmatag.markmytrainer.service.ReviewService;
import com.gyurmatag.markmytrainer.service.TrainerService;
import com.gyurmatag.markmytrainer.service.UserService;
import com.gyurmatag.markmytrainer.web.FlashMessage;
import com.gyurmatag.markmytrainer.web.error.NotCurrentUserReviewException;
import com.gyurmatag.markmytrainer.web.error.OneReviewAlreadyExistByTheCurrentUserException;
import com.gyurmatag.markmytrainer.web.error.ReviewNotFoundException;
import com.gyurmatag.markmytrainer.web.error.TrainerNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.Instant;
import java.util.Optional;

@Controller
public class ReviewController extends BaseController {
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private TrainerService trainerService;

    @Autowired
    private UserService userService;

    // Reviews by the current logged in user
    @GetMapping("user/reviews")
    public ModelAndView listReviewsByUser(@RequestParam("pageSize") Optional<Integer> pageSize,
                                          @RequestParam("page") Optional<Integer> page) {
        ModelAndView modelAndView = new ModelAndView("review/userReviews");

        evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        Page<Review> reviews = reviewService.findAllPageableByCreatedBy(new PageRequest(evalPage, evalPageSize));
        Pager pager = new Pager(reviews.getTotalPages(), reviews.getNumber(), BUTTONS_TO_SHOW);

        modelAndView.addObject("heading", "My reviews");
        modelAndView.addObject("reviews", reviews);
        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pager", pager);
        modelAndView.addObject("timestamp", Instant.now());

        return modelAndView;
    }

    // Add a review
    @RequestMapping(value = "/reviews", method = RequestMethod.POST)
    public String addReview(@Valid Review review, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            // Include validation errors upon redirect
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.review", result);

            // Add a review if invalid was received
            redirectAttributes.addFlashAttribute("review", review);

            // Redirect back to the form
            return "redirect:/trainers/" + review.getTrainer().getId() + "/review";
        }
        try {
            reviewService.save(review);
        } catch (OneReviewAlreadyExistByTheCurrentUserException oraebtcuex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage(oraebtcuex.getMessage(), FlashMessage.Status.DANGER));
            redirectAttributes.addFlashAttribute("review", review);
            return "redirect:/trainers/" + review.getTrainer().getId() + "/review";
        }

        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Review successfully added!", FlashMessage.Status.SUCCESS));
        return "redirect:/trainers/" + review.getTrainer().getId();
    }

    // Form for editing an existing review
    @RequestMapping(value = "/reviews/{reviewId}/edit")
    public String formEditReview(@PathVariable Long reviewId, Model model, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        if (!model.containsAttribute("review")) {
            Review review;

            try {
                review = reviewService.findByIdForEdit(reviewId);
            } catch (ReviewNotFoundException rnfex) {
                model.addAttribute("errorMsgReviewNotFound", "Nincs ilyen értékelés gec");
                return "error";
            }


            try {
                review = reviewService.findByIdForEdit(reviewId);
            } catch (NotCurrentUserReviewException ncurex) {
                redirectAttributes.addFlashAttribute("flash", new FlashMessage(ncurex.getMessage(), FlashMessage.Status.DANGER));
                return "redirect:/user/reviews";
            }
            model.addAttribute("review", reviewService.findById(reviewId));
        }

        backTimes = calculateBackTimes(request, true, ".*reviews/.*/edit");

        model.addAttribute("trainer", trainerService.findById(reviewService.findById(reviewId).getTrainer().getId()));
        model.addAttribute("action", String.format("/reviews/%s", reviewId));
        model.addAttribute("heading", "Edit review");
        model.addAttribute("submit", "Update");
        model.addAttribute("backTimes", backTimes);

        return "review/form";
    }

    // Update an existing review
    @Transactional
    @RequestMapping(value = "/reviews/{reviewId}", method = RequestMethod.POST)
    public String updateReview(@Valid Review review, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            // Include validation errors upon redirect
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.review", result);

            // Add a review if invalid was received
            redirectAttributes.addFlashAttribute("review", review);

            // Redirect back to the form
            return "redirect:/reviews/" + review.getId() + "/edit";
        }


        reviewService.edit(review);

        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Review updated successfully", FlashMessage.Status.SUCCESS));

        return "redirect:/user/reviews";
    }

    // Delete a review
    @Transactional
    @RequestMapping(value = "/deleteReview/{reviewId}", method = RequestMethod.GET)
    public String deleteReview(@PathVariable Long reviewId, Model model, RedirectAttributes redirectAttributes) {
        Review review;

        try {
            review = reviewService.findByIdForEdit(reviewId);
        } catch (ReviewNotFoundException rnfex) {
            model.addAttribute("errorMsgReviewNotFound", "Nincs ilyen értékelés gec");
            return "error";
        }

        try {
            reviewService.deleteById(reviewId);
        } catch (NotCurrentUserReviewException ncurex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage(ncurex.getMessage(), FlashMessage.Status.DANGER));
            return "redirect:/user/reviews";
        }

        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Review successfully deleted!", FlashMessage.Status.SUCCESS));
        return "redirect:/user/reviews";
    }

    // Form for uploading a new review to a given trainer
    @RequestMapping("/trainers/{trainerId}/review")
    public String formNewReviewTrainer(Model model, @PathVariable Long trainerId, HttpServletRequest request) {
        if (!model.containsAttribute("review")) {
            model.addAttribute("review", new Review());
        }

        backTimes = calculateBackTimes(request, true, ".*trainers/.*/review");

        Trainer trainer;

        try {
            trainer =  trainerService.findById(trainerId);
        } catch (TrainerNotFoundException tnfex) {
            model.addAttribute("errorMsgTrainerNotFound", "Nincs ilyen edző gec");
            return "error";
        }

        model.addAttribute("trainer", trainer);
        model.addAttribute("action", "/reviews");
        model.addAttribute("heading", "New review");
        model.addAttribute("submit", "Upload");
        model.addAttribute("backTimes", backTimes);

        return "review/form";
    }


}
