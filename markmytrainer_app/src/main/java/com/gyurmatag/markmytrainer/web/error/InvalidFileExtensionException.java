package com.gyurmatag.markmytrainer.web.error;

public class InvalidFileExtensionException extends RuntimeException {
    private static final long serialVersionUID = 5861310537366287163L;

    public InvalidFileExtensionException() {
        super();
    }

    public InvalidFileExtensionException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidFileExtensionException(final String message) {
        super(message);
    }

    public InvalidFileExtensionException(final Throwable cause) {
        super(cause);
    }
}
