package com.gyurmatag.markmytrainer.web.controller;

import com.gyurmatag.markmytrainer.model.Pager;
import com.gyurmatag.markmytrainer.model.Review;
import com.gyurmatag.markmytrainer.model.Trainer;
import com.gyurmatag.markmytrainer.service.CityService;
import com.gyurmatag.markmytrainer.service.GymService;
import com.gyurmatag.markmytrainer.service.ReviewService;
import com.gyurmatag.markmytrainer.service.TrainerService;
import com.gyurmatag.markmytrainer.web.FlashMessage;
import com.gyurmatag.markmytrainer.web.dto.AutocompleteForAllTrainersDto;
import com.gyurmatag.markmytrainer.web.dto.TrainerAddDto;
import com.gyurmatag.markmytrainer.web.error.InvalidFileExtensionException;
import com.gyurmatag.markmytrainer.web.error.TrainerNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Controller
public class TrainerController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TrainerService trainerService;

    @Autowired
    private GymService gymService;

    @Autowired
    private CityService cityService;

    @Autowired
    private ReviewService reviewService;

    @ModelAttribute("trainerAddForm")
    public TrainerAddDto trainerAddDto() {
        return new TrainerAddDto();
    }

    @GetMapping("/trainers")
    public ModelAndView listTrainers(@RequestParam("pageSize") Optional<Integer> pageSize,
                                     @RequestParam("page") Optional<Integer> page,
                                     @RequestParam("trainer") Optional<String> trainer,
                                     @RequestParam("gym") Optional<String> gym,
                                     @RequestParam("city") Optional<String> city,
                                     @RequestParam("sortOption") Optional<String> sortOption) {
        ModelAndView modelAndView = new ModelAndView("trainer/trainers");

        evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        Page<Trainer> trainers = null;

        String sortString = sortOption.orElse("");

        if(sortString.equals("Legnépszerűbb")) {
            trainers = trainerService.findAllAdvancedSortedByAvgRatingDesc(trainer, gym, city, new PageRequest(evalPage, evalPageSize));
        } else if(sortString.equals("Legutáltabb")) {
            trainers = trainerService.findAllAdvancedSortedByAvgRatingAsc(trainer, gym, city, new PageRequest(evalPage, evalPageSize));
        } else if(sortString.equals("Legtöbb értékelés")) {
            trainers = trainerService.findAllAdvancedSortedByRatingNumberDesc(trainer, gym, city, new PageRequest(evalPage, evalPageSize));
        } else if(sortString.equals("")){
            trainers = trainerService.findAllAdvancedSortedByAvgRatingDesc(trainer, gym, city, new PageRequest(evalPage, evalPageSize));
        } else {
            trainers = trainerService.findAllAdvancedSortedByAvgRatingDesc(trainer, gym, city, new PageRequest(evalPage, evalPageSize));
        }

        String searchTrainer = trainer.orElse("");
        String searchGym = gym.orElse("");
        String sarchCity = city.orElse("");

        if(!searchTrainer.equals("") && searchGym.equals("") && sarchCity.equals("")) {
            modelAndView.addObject("heading", "Edzők ezzel a névvel: \"" + searchTrainer + "\"");
        } else if(searchTrainer.equals("") && !searchGym.equals("") && sarchCity.equals("")) {
            modelAndView.addObject("heading", "Edzők ebben a teremben: \"" + searchGym + "\"");
        } else if(searchTrainer.equals("") && searchGym.equals("") && !sarchCity.equals("")) {
            modelAndView.addObject("heading", "Edzők ebben a városban: \"" + sarchCity + "\"");
        } else if(!searchTrainer.equals("") && !searchGym.equals("") && sarchCity.equals("")) {
            modelAndView.addObject("heading", "Edzők ebben a teremben: \"" + searchGym + "\" ezzel a névvel: \"" + searchTrainer + "\"");
        } else if(searchTrainer.equals("") && !searchGym.equals("") && !sarchCity.equals("")) {
            modelAndView.addObject("heading", "Edzők ebben a teremben: \"" + searchGym + "\" , ebben a városban: \"" + sarchCity + "\"");
        } else if(!searchTrainer.equals("") && searchGym.equals("") && !sarchCity.equals("")) {
            modelAndView.addObject("heading", "Edzők ebben a városban: \"" + sarchCity + "\" , ezzel a névvel: \"" + searchTrainer + "\"");
        } else if(!searchTrainer.equals("") && !searchGym.equals("") && !sarchCity.equals("")) {
            modelAndView.addObject("heading", "Edzők ebben a teremben: \"" + searchGym + "\", ebben a városban: \"" + sarchCity + "\" , ezzel a névvel: \"" + searchTrainer + "\"");
        } else {
            modelAndView.addObject("heading", "Trainers");
        }

        Pager pager = new Pager(trainers.getTotalPages(), trainers.getNumber(), BUTTONS_TO_SHOW);

        modelAndView.addObject("sortOptions", orderByOptionsForTrainer());
        modelAndView.addObject("selector", sortString);

        modelAndView.addObject("trainer", searchTrainer);
        modelAndView.addObject("trainerCount", trainers.getTotalElements());
        modelAndView.addObject("gym", searchGym);
        modelAndView.addObject("city", sarchCity);
        modelAndView.addObject("trainers", trainers);
        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pager", pager);

        return modelAndView;
    }


    @GetMapping("/trainers/{trainerId}")
    public ModelAndView listReviewsOnTrainerPage(@PathVariable Long trainerId,
                                                 @RequestParam("pageSize") Optional<Integer> pageSize,
                                                 @RequestParam("page") Optional<Integer> page,
                                                 @RequestParam("reviewSort") Optional<String> reviewSort) {
        ModelAndView modelAndView = new ModelAndView("trainer/details");

        Trainer trainer;

        try {
            trainer = trainerService.findById(trainerId);
        } catch (TrainerNotFoundException tnfex) {
            ModelAndView modelAndViewException = new ModelAndView("error");
            modelAndViewException.addObject("errorMsgTrainerNotFound", "Nincs ilyen edző gec");
            return modelAndViewException;
        }

        evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        Page<Review> reviews;

        String reviewSortString = reviewSort.orElse("");

        if(reviewSortString.equals("Növekvő")) {
            reviews = reviewService.findAllPageableByTrainerIdOrderByRatingAsc(trainerId, new PageRequest(evalPage, evalPageSize));
        } else if(reviewSortString.equals("Csökkenő")) {
            reviews = reviewService.findAllPageableByTrainerIdOrderByRatingDesc(trainerId, new PageRequest(evalPage, evalPageSize));
        } else if(reviewSortString.equals("")){
            reviews = reviewService.findAllPageableByTrainerIdOrderByRatingDesc(trainerId, new PageRequest(evalPage, evalPageSize));
        } else {
            reviews = reviewService.findAllPageableByTrainerIdOrderByRatingDesc(trainerId, new PageRequest(evalPage, evalPageSize));
        }

        Pager pager = new Pager(reviews.getTotalPages(), reviews.getNumber(), BUTTONS_TO_SHOW);

        Double avgRating = 0.0;
        int countRating = trainerService.getCountRatingById(trainerId);

        if(countRating > 0) {
            avgRating = trainerService.getAverageRatingById(trainerId);
        }

        modelAndView.addObject("trainer", trainer);
        modelAndView.addObject("countRating", countRating);
        modelAndView.addObject("avgRating", avgRating);
        modelAndView.addObject("reviews", reviews);
        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pager", pager);
        return modelAndView;
    }


    // Upload a new trainer
    @RequestMapping(value = "/trainers", method = RequestMethod.POST)
    @Transactional
    public String addTrainer(@ModelAttribute("trainerAddForm") @Valid TrainerAddDto trainerAddDto, BindingResult result,
                             @RequestParam MultipartFile file, RedirectAttributes redirectAttributes) throws IOException {
        if (result.hasErrors()) {
            // Include validation errors upon redirect
            redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".trainerAddForm", result);

            // Add a trainer if invalid was received
            redirectAttributes.addFlashAttribute("trainerAddForm", trainerAddDto);

            // Redirect back to the form
            return "redirect:/trainers/add";
        }

        Long redirectId = null;

        try {
            redirectId = trainerService.save(trainerAddDto, file);
        } catch (InvalidFileExtensionException ifeex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage(ifeex.getMessage(), FlashMessage.Status.DANGER));
            return "redirect:/trainers/add";
        }

        // Add flash message for success
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Trainer successfully created!", FlashMessage.Status.SUCCESS));
        return String.format("redirect:/trainers/%s", redirectId);
    }


    // Form for uploading a new trainer
    @RequestMapping("/trainers/add")
    public String formNewTrainer(Model model, HttpServletRequest request) {
        //TODO: hogyha minden kötél szakad tereljen vissza a BACK button a főoldalra!!!

        if (!model.containsAttribute("trainer")) {
            model.addAttribute("trainer", new Trainer());
        }

        backTimes = calculateBackTimes(request, false, "trainers/add");

        model.addAttribute("action", "/trainers");
        model.addAttribute("heading", "Upload");
        model.addAttribute("submit", "Upload");
        model.addAttribute("backTimes", backTimes);

        return "trainer/form";
    }

    // Upload a new trainer to a given gym
    @RequestMapping(value = "gyms/addTrainer", method = RequestMethod.POST)
    @Transactional
    public String addTrainerToGym(@ModelAttribute("trainerAddForm") @Valid TrainerAddDto trainerAddDto, BindingResult result, @RequestParam MultipartFile file, RedirectAttributes redirectAttributes) throws IOException {
        if (result.hasErrors()) {
            // Include validation errors upon redirect
            redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".trainerAddForm", result);

            // Add a trainer if invalid was received
            redirectAttributes.addFlashAttribute("trainerAddForm", trainerAddDto);

            // Redirect back to the form
            return "redirect:/gyms/" + trainerAddDto.getGym().getId() + "/addTrainer";
        }

        Long redirectId = null;

        try {
            redirectId = trainerService.save(trainerAddDto, file);
        } catch (InvalidFileExtensionException ifeex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage(ifeex.getMessage(), FlashMessage.Status.DANGER));
            return "redirect:/gyms/" + trainerAddDto.getGym().getId() + "/addTrainer";
        }

        // Add flash message for success
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Trainer successfully created!", FlashMessage.Status.SUCCESS));
        return String.format("redirect:/trainers/%s", redirectId);
    }

    // Form for uploading a new trainer to a given gym
    @RequestMapping("/gyms/{gymId}/addTrainer")
    public String formNewTrainerToGym(Model model, @PathVariable Long gymId, HttpServletRequest request) {
        //TODO: hogyha minden kötél szakad tereljen vissza a BACK button a főoldalra!!!

        if (!model.containsAttribute("trainer")) {
            model.addAttribute("trainer", new Trainer());
        }

        backTimes = calculateBackTimes(request, true, ".*gyms/.*/addTrainer");

        model.addAttribute("gym", gymService.findById(gymId));
        model.addAttribute("action", "/gyms/addTrainer");
        model.addAttribute("heading", "Upload");
        model.addAttribute("submit", "Upload");
        model.addAttribute("backTimes", backTimes);

        return "gym/trainerForm";
    }

    @ResponseBody
    @RequestMapping(value = "/autocompleteTrainersAll", method = RequestMethod.GET)
    public Map<String, List<AutocompleteForAllTrainersDto>> autocompleteTrainersAll(@RequestParam("searchstr") String searchstr) {
        Map<String, List<AutocompleteForAllTrainersDto>> map = new HashMap<>();

        List<AutocompleteForAllTrainersDto> trainerListAll = trainerService.findTop5ByNameContainsIgnoreCase(searchstr.trim());

        map.put("trainerListAll", trainerListAll);
        return map;
    }

}
