package com.gyurmatag.markmytrainer.web.controller;

import com.gyurmatag.markmytrainer.model.Contact;
import com.gyurmatag.markmytrainer.model.Trainer;
import com.gyurmatag.markmytrainer.service.ContactService;
import com.gyurmatag.markmytrainer.web.FlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

@Controller
public class SupportController extends BaseController {

    @Autowired
    ContactService contactService;

    @GetMapping("/faq")
    public ModelAndView faq() {
        ModelAndView modelAndView = new ModelAndView("support/faq");

        return modelAndView;
    }

    // Form for adding a contact message
    @RequestMapping("/contact")
    public String formNewGym(Model model, HttpServletRequest request) {
        if (!model.containsAttribute("contact")) {
            model.addAttribute("contact", new Contact());
        }

        backTimes = calculateBackTimes(request, false, "contact");

        model.addAttribute("action", "/contact");
        model.addAttribute("backTimes", backTimes);

        return "support/contact";
    }

    // Upload a new support message
    @RequestMapping(value = "/contact", method = RequestMethod.POST)
    @Transactional
    public String addTrainer(@ModelAttribute("contact") @Valid Contact contact, BindingResult result, RedirectAttributes redirectAttributes) throws IOException {
        if (result.hasErrors()) {
            // Include validation errors upon redirect
            redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".contact", result);

            // Add a trainer if invalid was received
            redirectAttributes.addFlashAttribute("contact", contact);

            // Redirect back to the form
            return "redirect:/contact";
        }

        contactService.save(contact);
        contactService.sendMailToSupport(contact.getName(), contact.getEmail(), contact.getMessage());

        // Add flash message for success
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Support mail sent! Thank you!", FlashMessage.Status.SUCCESS));
        return String.format("redirect:/contact");
    }

}
