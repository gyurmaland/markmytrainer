package com.gyurmatag.markmytrainer.web.error;

public final class UserEmailAlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = 5861310537366287163L;

    public UserEmailAlreadyExistException() {
        super();
    }

    public UserEmailAlreadyExistException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UserEmailAlreadyExistException(final String message) {
        super(message);
    }

    public UserEmailAlreadyExistException(final Throwable cause) {
        super(cause);
    }

}
