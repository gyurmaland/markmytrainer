package com.gyurmatag.markmytrainer.web.error;

public class GymNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 5861310537366287163L;

    public GymNotFoundException() {
        super();
    }

    public GymNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GymNotFoundException(final String message) {
        super(message);
    }

    public GymNotFoundException(final Throwable cause) {
        super(cause);
    }
}
