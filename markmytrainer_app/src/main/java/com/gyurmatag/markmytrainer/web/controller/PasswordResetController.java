package com.gyurmatag.markmytrainer.web.controller;

import com.gyurmatag.markmytrainer.model.PasswordResetToken;
import com.gyurmatag.markmytrainer.model.User;
import com.gyurmatag.markmytrainer.service.PasswordResetTokenService;
import com.gyurmatag.markmytrainer.service.UserService;
import com.gyurmatag.markmytrainer.web.FlashMessage;
import com.gyurmatag.markmytrainer.web.dto.PasswordResetDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Locale;

@Controller
public class PasswordResetController {
    private final Log log = LogFactory.getLog(this.getClass());
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordResetTokenService passwordResetTokenService;
    @Autowired
    private MessageSource messageSource;

    @ModelAttribute("passwordResetForm")
    public PasswordResetDto passwordReset() {
        return new PasswordResetDto();
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
    public String forgotPasswordForm(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getPrincipal() instanceof UserDetails) {
            return "redirect:/trainers";
        }

        model.addAttribute("heading", "Reset Password");
        model.addAttribute("action", "/resetPassword");

        return "authentication/resetPassword";
    }

    @RequestMapping(path = "/resetPassword/{token}", method = RequestMethod.GET)
    public String forgotPasswordForm(@PathVariable("token") String token, Model model, RedirectAttributes redirectAttributes) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getPrincipal() instanceof UserDetails) {
            return "redirect:/trainers";
        }

        if (!model.containsAttribute("user")) {
            model.addAttribute("user", new User());
        }
        model.addAttribute("heading", "Reset Password");
        model.addAttribute("action", "/resetPassword");

        PasswordResetToken resetToken = passwordResetTokenService.findByToken(token);
        if (resetToken == null) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("Token invalid", FlashMessage.Status.DANGER));
            log.error("invalid token");
            return "redirect:/forgotPassword";
        } else if (resetToken.isExpired()) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("Token expired", FlashMessage.Status.DANGER));
            log.error("expried token");
            return "redirect:/forgotPassword";
        }

        model.addAttribute("token", resetToken.getToken());

        return "authentication/resetPassword";
    }


    @Transactional
    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
    public String processForgotPasswordForm(@ModelAttribute("passwordResetForm") @Valid PasswordResetDto passwordResetDto, BindingResult result, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".passwordResetForm", result);
            redirectAttributes.addFlashAttribute("passwordResetForm", passwordResetDto);

            return "redirect:/resetPassword/" + passwordResetDto.getToken();
        }

        PasswordResetToken passwordResetToken = passwordResetTokenService.findByToken(passwordResetDto.getToken());
        User user = passwordResetToken.getUser();
        userService.updatePassword(passwordResetDto.getPassword(), user.getId());
        passwordResetTokenService.delete(passwordResetToken);

        redirectAttributes.addFlashAttribute("flash", new FlashMessage(messageSource.getMessage("password.changed.success", null, Locale.US), FlashMessage.Status.SUCCESS));

        return "redirect:/login";

    }

}
