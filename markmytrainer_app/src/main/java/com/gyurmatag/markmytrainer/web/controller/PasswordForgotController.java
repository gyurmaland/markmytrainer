package com.gyurmatag.markmytrainer.web.controller;

import com.gyurmatag.markmytrainer.model.PasswordResetToken;
import com.gyurmatag.markmytrainer.model.User;
import com.gyurmatag.markmytrainer.service.MailClientService;
import com.gyurmatag.markmytrainer.service.PasswordResetTokenService;
import com.gyurmatag.markmytrainer.service.UserService;
import com.gyurmatag.markmytrainer.web.FlashMessage;
import com.gyurmatag.markmytrainer.web.dto.PasswordForgotDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@Controller
public class PasswordForgotController extends BaseController {
    private final Log log = LogFactory.getLog(this.getClass());
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordResetTokenService passwordResetTokenService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private MailClientService mailClientService;

    @Value("${spring.mail.pwReset}")
    private String WEB_APP_URL_FOR_PWRESET;

    @ModelAttribute("forgotPasswordForm")
    public PasswordForgotDto forgotPasswordDto() {
        return new PasswordForgotDto();
    }

    @RequestMapping(value = "/forgotPassword", method = RequestMethod.GET)
    public String forgotPasswordForm(Model model, HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getPrincipal() instanceof UserDetails) {
            return "redirect:/trainers";
        }

        backTimes = calculateBackTimes(request, false, "forgotPassword");

        model.addAttribute("heading", "Forgot Password");
        model.addAttribute("action", "/forgotPassword");
        model.addAttribute("backTimes", backTimes);

        return "authentication/forgotPassword";
    }

    @Transactional
    @RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
    public String processForgotPasswordForm(@ModelAttribute("forgotPasswordForm") @Valid PasswordForgotDto passwordForgotDto, BindingResult result, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".forgotPasswordForm", result);
            redirectAttributes.addFlashAttribute("forgotPasswordForm", passwordForgotDto);

            return "redirect:/forgotPassword";
        }

        User user = userService.findByEmail(passwordForgotDto.getEmail());
        if (user == null) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("This email not found", FlashMessage.Status.DANGER));
            redirectAttributes.addFlashAttribute("forgotPasswordForm", passwordForgotDto);
            return "redirect:/forgotPassword";
        }

        PasswordResetToken passwordResetToken = passwordResetTokenService.generatePasswordResetTokenForUser(user);
        passwordResetTokenService.save(passwordResetToken);

        mailClientService.prepareAndSendPwResetEmail(user.getEmail(), "Jelszó helyreállítása" ,WEB_APP_URL_FOR_PWRESET + passwordResetToken.getToken());

        redirectAttributes.addFlashAttribute("flash", new FlashMessage(messageSource.getMessage("forgot.password.link.sent.success", new Object[]{user.getEmail()}, Locale.US), FlashMessage.Status.SUCCESS));

        return "redirect:/login";

    }


}
