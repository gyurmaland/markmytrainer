package com.gyurmatag.markmytrainer.web.controller;

import com.gyurmatag.markmytrainer.model.Trainer;
import com.gyurmatag.markmytrainer.service.TrainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
public class HomeController {

    @Autowired
    private TrainerService trainerService;


    @GetMapping("/")
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView("home/home");

        Page<Trainer> top3Trainers = trainerService.findAllAdvancedSortedByAvgRatingDesc(Optional.empty(),Optional.empty(),Optional.empty(),new PageRequest(0, 3));

        modelAndView.addObject("top3Trainers", top3Trainers);

        return modelAndView;
    }
}
