package com.gyurmatag.markmytrainer.web.controller;


import com.gyurmatag.markmytrainer.model.Gym;
import com.gyurmatag.markmytrainer.model.Pager;
import com.gyurmatag.markmytrainer.model.QGym;
import com.gyurmatag.markmytrainer.model.Trainer;
import com.gyurmatag.markmytrainer.service.CityService;
import com.gyurmatag.markmytrainer.service.GymService;
import com.gyurmatag.markmytrainer.service.TrainerService;
import com.gyurmatag.markmytrainer.web.FlashMessage;
import com.gyurmatag.markmytrainer.web.dto.AutocompleteForAllGymsDto;
import com.gyurmatag.markmytrainer.web.dto.GymAddDto;
import com.gyurmatag.markmytrainer.web.error.GymNotFoundException;
import com.gyurmatag.markmytrainer.web.error.InvalidFileExtensionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.data.querydsl.QSort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

@Controller
public class GymController extends BaseController {

    @Autowired
    private GymService gymService;
    @Autowired
    private TrainerService trainerService;
    @Autowired
    private CityService cityService;

    public GymController() {
        super();
    }

    @ModelAttribute("gymAddForm")
    public GymAddDto gymAddDto() {
        return new GymAddDto();
    }

    @GetMapping("/gyms")
    public ModelAndView listGyms(@RequestParam("pageSize") Optional<Integer> pageSize,
                                 @RequestParam("page") Optional<Integer> page,
                                 @RequestParam("gymName") Optional<String> gymName,
                                 @RequestParam("cityName") Optional<String> cityName,
                                 @RequestParam("sortOption") Optional<String> sortOption) {
        ModelAndView modelAndView = new ModelAndView("gym/gyms");

        evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        boolean isQueryDsl = true;
        String sortString = sortOption.orElse("");

        Page<Gym> gyms = null;

        if(sortString.equals("Legjobb értékelések")) {
            isQueryDsl = false;
            gyms = gymService.findAllAdvancedSortedByAvgTrainerRatingDesc(gymName, cityName, new PageRequest(evalPage, evalPageSize));
        } else if(sortString.equals("Legrosszabb értékelések")) {
            isQueryDsl = false;
            gyms = gymService.findAllAdvancedSortedByAvgTrainerRatingAsc(gymName, cityName, new PageRequest(evalPage, evalPageSize));
        } else {
            isQueryDsl = true;
            gyms = gymService.findAllAdvanced(gymName, cityName, new QPageRequest(evalPage, evalPageSize, new QSort(QGym.gym.trainers.size().desc())));
        }

        String searchCity = cityName.orElse("");
        searchName = gymName.orElse("");

        if (!searchName.equals("") && searchCity.equals("")) {
            modelAndView.addObject("heading", "Keresési találatok erre a névre: \"" + searchName + "\"");
            modelAndView.addObject("searchByName", searchName);
        } else if (searchName.equals("") && !searchCity.equals("")) {
            modelAndView.addObject("heading", "Termek ebben a városban: \"" + searchCity + "\"");
            modelAndView.addObject("searchByCity", searchCity);
        } else if (searchName.equals("") && !searchCity.equals("")) {
            modelAndView.addObject("heading", "Termek ebben a városban: \"" + searchCity + "\"" + " ezzel a névvel: " + "\"" + searchName + "\"");
            modelAndView.addObject("searchByName", searchName);
            modelAndView.addObject("searchByCity", searchCity);
        } else {
            modelAndView.addObject("heading", "Gyms");
        }

        Pager pager = new Pager(gyms.getTotalPages(), gyms.getNumber(), BUTTONS_TO_SHOW);

        modelAndView.addObject("sortOptions", orderByOptionsForGym());
        modelAndView.addObject("isQueryDsl", isQueryDsl);
        modelAndView.addObject("selector", sortString);

        modelAndView.addObject("gyms", gyms);
        modelAndView.addObject("gymCount", gyms.getTotalElements());
        modelAndView.addObject("gymName", searchName);
        modelAndView.addObject("cityName", searchCity);
        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pager", pager);
        return modelAndView;
    }

    @GetMapping("/gyms/{gymId}")
    public ModelAndView listTrainers(@PathVariable Long gymId,
                                     @RequestParam("pageSize") Optional<Integer> pageSize,
                                     @RequestParam("page") Optional<Integer> page) {
        ModelAndView modelAndView = new ModelAndView("gym/details");

        Gym gym;

        try  {
            gym = gymService.findById(gymId);
        } catch (GymNotFoundException gnfex) {
            ModelAndView modelAndViewException = new ModelAndView("error");
            modelAndViewException.addObject("errorMsgGymNotFound", "Nincs ilyen terem gec");
            return modelAndViewException;
        }


        evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        Page<Trainer> trainers = trainerService.findAllPageableByGymIdSortedByAvgRatingDesc(gym.getId(), new PageRequest(evalPage, evalPageSize));

        int countRating = gymService.getTrainerReviewCount(gym.getId());

        Double trainerAvgScore = gymService.getTrainerScoreAvg(gym.getId());


        Pager pager = new Pager(trainers.getTotalPages(), trainers.getNumber(), BUTTONS_TO_SHOW);

        modelAndView.addObject("heading", "Trainers in " + gym.getName());
        modelAndView.addObject("gym", gym);
        modelAndView.addObject("gymId", gymId);
        modelAndView.addObject("trainers", trainers);
        modelAndView.addObject("trainersNumber", trainers.getTotalElements());
        modelAndView.addObject("countRating", countRating);
        modelAndView.addObject("trainerAvgScore", trainerAvgScore);
        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pager", pager);
        return modelAndView;
    }

    // Form for adding a new gym
    @RequestMapping("gyms/add")
    public String formNewGym(Model model, HttpServletRequest request) {
        if (!model.containsAttribute("gym")) {
            model.addAttribute("gym", new Gym());
        }

        backTimes = calculateBackTimes(request, false, "gyms/add");

        model.addAttribute("action", "/gyms");
        model.addAttribute("heading", "New Gym");
        model.addAttribute("submit", "Add");
        model.addAttribute("backTimes", backTimes);

        return "gym/form";
    }

    // Add a gym
    @RequestMapping(value = "/gyms", method = RequestMethod.POST)
    public String addGym(@ModelAttribute("gymAddForm") @Valid GymAddDto gymAddDto, BindingResult result, @RequestParam MultipartFile file, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            // Include validation errors upon redirect
            redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".gymAddForm", result);

            // Add a gym if invalid was received
            redirectAttributes.addFlashAttribute("gymAddForm", gymAddDto);

            // Redirect back to the form
            return "redirect:/gyms/add";
        }

        Long redirectId = null;

        try {
            redirectId =  gymService.save(cityService.addIfNotExistSetIfExistsAtGyms(gymAddDto), file);
        } catch (InvalidFileExtensionException ifeex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage(ifeex.getMessage(), FlashMessage.Status.DANGER));
            return "redirect:/gyms/add";
        }


        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Gym successfully added!", FlashMessage.Status.SUCCESS));

        return String.format("redirect:/gyms/%s", redirectId);
    }

    @ResponseBody
    @RequestMapping(value = "/gyms/{gymId}/searchTrainer", method = RequestMethod.GET)
    public ModelAndView searchTrainersInGym(@PathVariable Long gymId,
                                            @RequestParam("trainerName")  Optional<String> trainerName,
                                            @RequestParam("pageSize") Optional<Integer> pageSize,
                                            @RequestParam("page") Optional<Integer> page) {
        ModelAndView modelAndView = new ModelAndView("gym/details");

        Gym gym = gymService.findById(gymId);

        int countRating = gymService.getTrainerReviewCount(gym.getId());

        Double trainerAvgScore = gymService.getTrainerScoreAvg(gym.getId());

        int trainerCount = gymService.getTrainerCount(gym.getId());

        Optional<String> gymName = Optional.of(gym.getName());
        evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        Page<Trainer> trainers = trainerService.findAllAdvancedSortedByAvgRatingDesc(trainerName, gymName, Optional.empty(),  new PageRequest(evalPage, evalPageSize));
        Pager pager = new Pager(trainers.getTotalPages(), trainers.getNumber(), BUTTONS_TO_SHOW);


        modelAndView.addObject("heading", "Trainer search results in \"" + gymService.findById(gymId).getName() + "\"" + " for \"" + trainerName.orElse("") + "\"");
        modelAndView.addObject("trainerName", trainerName.orElse(""));
        modelAndView.addObject("gymId", gymId);
        modelAndView.addObject("trainers", trainers);
        modelAndView.addObject("countRating", countRating);
        modelAndView.addObject("trainerAvgScore", trainerAvgScore);
        modelAndView.addObject("trainersNumber", trainerCount);
        modelAndView.addObject("gym", gym);
        modelAndView.addObject("selectedPageSize", evalPageSize);
        modelAndView.addObject("pager", pager);

        modelAndView.addObject("popularSort", "none");
        modelAndView.addObject("sortButtonVisible", "none");

        return modelAndView;
    }

    @ResponseBody
    @RequestMapping(value = "trainers/gymsByCityName", method = RequestMethod.GET)
    public List<Gym> gymsByCityName(@RequestParam("cityName") String cityName) {

        if (cityService.findByNameIgnoreCase(cityName.trim()) == null) {
            return Collections.<Gym>emptyList();
        }

        Long cityId = cityService.findByNameIgnoreCase(cityName.trim()).getId();

        return gymService.findByCityId(cityId);
    }

    @ResponseBody
    @RequestMapping(value = "/autocompleteGymsAll", method = RequestMethod.GET)
    public Map<String, List<AutocompleteForAllGymsDto>> autocompleteGymsAll(@RequestParam("searchstr") String searchstr) {

        Map<String, List<AutocompleteForAllGymsDto>> map = new HashMap<>();

        List<AutocompleteForAllGymsDto> gymListAll = gymService.findTop5ByNameContainsIgnoreCase(searchstr.trim());

        map.put("gymListAll", gymListAll);
        return map;
    }


    @ResponseBody
    @RequestMapping(value = "/autocompleteTrainersInGym/{gymId}", method = RequestMethod.GET)
    public Map<String, List<Trainer>> autoCompleteTrainersInGym(@PathVariable Long gymId, @RequestParam("searchstr") String searchstr) {
        Map<String, List<Trainer>> map = new HashMap<>();

        List<Trainer> trainerListInGym = trainerService.findTop5ByNameContainsIgnoreCaseAndGymId(searchstr.trim(), gymId);

        map.put("trainerListInGym", trainerListInGym);
        return map;
    }

}
