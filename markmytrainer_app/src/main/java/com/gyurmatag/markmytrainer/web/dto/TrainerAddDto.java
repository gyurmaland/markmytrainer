package com.gyurmatag.markmytrainer.web.dto;

import com.gyurmatag.markmytrainer.model.Gym;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TrainerAddDto {

    @NotNull
    @Size(min = 3, max = 100, message = "{trainer.name.size}")
    private String name;

    @NotNull
    @Size(min = 2, max = 100, message = "{city.name.size}")
    private String cityName;

    @ManyToOne
    @NotNull(message = "{trainer.gym.exist}")
    private Gym gym;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Gym getGym() {
        return gym;
    }

    public void setGym(Gym gym) {
        this.gym = gym;
    }
}
