package com.gyurmatag.markmytrainer.web.error;

public class BadOldPasswordException extends RuntimeException {
    private static final long serialVersionUID = 5861310537366287163L;

    public BadOldPasswordException() {
        super();
    }

    public BadOldPasswordException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public BadOldPasswordException(final String message) {
        super(message);
    }

    public BadOldPasswordException(final Throwable cause) {
        super(cause);
    }
}
