package com.gyurmatag.markmytrainer.web.dto;

import com.gyurmatag.markmytrainer.validation.FieldMatch;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@FieldMatch(first = "password", second = "confirmPassword", message = "{forgot.password.password.match}")
public class NewPasswordDto {

    @NotEmpty(message = "{user.password.oldPassword}")
    private String oldPassword;

    @Size(min = 6, message = "{user.password.valid}")
    private String newPassword;

    @NotEmpty(message = "{user.password.confirmNew}")
    private String confirmNewPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }

}
