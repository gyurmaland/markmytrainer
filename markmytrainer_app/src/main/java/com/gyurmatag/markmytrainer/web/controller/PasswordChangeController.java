package com.gyurmatag.markmytrainer.web.controller;

import com.gyurmatag.markmytrainer.model.User;
import com.gyurmatag.markmytrainer.service.UserService;
import com.gyurmatag.markmytrainer.web.FlashMessage;
import com.gyurmatag.markmytrainer.web.dto.NewPasswordDto;
import com.gyurmatag.markmytrainer.web.error.BadOldPasswordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@Controller
public class PasswordChangeController extends BaseController {

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSource messageSource;

    @ModelAttribute("passwordChangeForm")
    public NewPasswordDto passwordChange() {
        return new NewPasswordDto();
    }

    @RequestMapping(value = "/user/newPassword", method = RequestMethod.GET)
    public String forgotPasswordForm(Model model, HttpServletRequest request) {
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", new User());
        }

        backTimes = calculateBackTimes(request, false, "newPassword");

        model.addAttribute("heading", "New Password");
        model.addAttribute("action", "/newPassword");
        model.addAttribute("backTimes", backTimes);

        return "profile/newPassword";
    }


    @Transactional
    @RequestMapping(value = "/newPassword", method = RequestMethod.POST)
    public String processForgotPasswordForm(@ModelAttribute("passwordChangeForm") @Valid NewPasswordDto newPasswordDto, BindingResult result, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".passwordChangeForm", result);
            redirectAttributes.addFlashAttribute("passwordChangeForm", newPasswordDto);

            return "redirect:/user/newPassword";
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(auth.getName());
        try {
            userService.changePassword(newPasswordDto.getOldPassword(), newPasswordDto.getNewPassword(), user.getId());
        } catch (BadOldPasswordException bopwex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage(bopwex.getMessage(), FlashMessage.Status.DANGER));
            return "redirect:/user/newPassword";
        }

        redirectAttributes.addFlashAttribute("flash", new FlashMessage(messageSource.getMessage("password.changed.success", null, Locale.US), FlashMessage.Status.SUCCESS));

        return "redirect:/user/profile";

    }
}
