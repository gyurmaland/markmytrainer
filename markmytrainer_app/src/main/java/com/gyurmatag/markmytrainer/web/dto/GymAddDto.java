package com.gyurmatag.markmytrainer.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class GymAddDto {

    @NotNull
    @Size(min = 3, max = 100, message = "{gym.name.size}")
    private String name;

    @NotNull
    @Size(min = 2, max = 100, message = "{city.name.size}")
    private String cityName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
