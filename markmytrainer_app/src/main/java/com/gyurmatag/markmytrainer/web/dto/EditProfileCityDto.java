package com.gyurmatag.markmytrainer.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EditProfileCityDto {

    private Long id;

    @NotNull
    @Size(min = 2, max = 100, message = "{city.name.size}")
    private String cityName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}

