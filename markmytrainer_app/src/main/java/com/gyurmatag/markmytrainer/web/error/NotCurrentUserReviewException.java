package com.gyurmatag.markmytrainer.web.error;

public class NotCurrentUserReviewException extends RuntimeException {
    private static final long serialVersionUID = 5861310537366287163L;

    public NotCurrentUserReviewException() {
        super();
    }

    public NotCurrentUserReviewException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public NotCurrentUserReviewException(final String message) {
        super(message);
    }

    public NotCurrentUserReviewException(final Throwable cause) {
        super(cause);
    }
}
