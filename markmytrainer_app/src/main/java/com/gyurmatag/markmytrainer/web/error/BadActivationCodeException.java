package com.gyurmatag.markmytrainer.web.error;

public final class BadActivationCodeException extends RuntimeException {
    private static final long serialVersionUID = 5861310537366287163L;

    public BadActivationCodeException() {
        super();
    }

    public BadActivationCodeException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public BadActivationCodeException(final String message) {
        super(message);
    }

    public BadActivationCodeException(final Throwable cause) {
        super(cause);
    }
}
