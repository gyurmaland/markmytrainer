package com.gyurmatag.markmytrainer.web.error;

public class ReviewNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 5861310537366287163L;

    public ReviewNotFoundException() {
        super();
    }

    public ReviewNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ReviewNotFoundException(final String message) {
        super(message);
    }

    public ReviewNotFoundException(final Throwable cause) {
        super(cause);
    }
}
