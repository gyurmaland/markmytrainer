package com.gyurmatag.markmytrainer.web.dto;

import com.gyurmatag.markmytrainer.validation.FieldMatch;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@FieldMatch(first = "password", second = "confirmPassword", message = "{signup.password.match}")
public class SignUpDto {

    @Size(min = 3, max = 20, message = "{user.name.size}")
    private String username;

    @NotEmpty(message = "{user.email.exist}")
    @Email(message = "{user.email.valid}")
    private String email;

    @Column(length = 100)
    @Size(min = 6, message = "{user.password.valid}")
    private String password;

    @NotEmpty
    private String confirmPassword;

    @NotNull
    @Size(min = 2, max = 100, message = "{city.name.size}")
    private String cityName;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
