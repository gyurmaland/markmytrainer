package com.gyurmatag.markmytrainer.web.controller;

import com.gyurmatag.markmytrainer.model.User;
import com.gyurmatag.markmytrainer.service.CityService;
import com.gyurmatag.markmytrainer.service.UserService;
import com.gyurmatag.markmytrainer.web.FlashMessage;
import com.gyurmatag.markmytrainer.web.dto.EditProfileCityDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class ProfileController extends BaseController {

    @Autowired
    private CityService cityService;

    @Autowired
    private UserService userService;

    @ModelAttribute("editProfileCityForm")
    public EditProfileCityDto editProfileDto() {
        return new EditProfileCityDto();
    }

    @GetMapping("user/profile")
    public ModelAndView profileDetail(Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView("profile/details");

        modelAndView.addObject("username", authentication.getName());

        return modelAndView;
    }

    @RequestMapping("user/editCity")
    public String profileEditForm(Model model, HttpServletRequest request, Authentication authentication) {

        model.addAttribute("editProfileCityForm", userService.findByUserNameForEdit(authentication.getName()));

        backTimes = calculateBackTimes(request, false, "editCity");

        model.addAttribute("action", "/editCity");
        model.addAttribute("backTimes", backTimes);

        return "profile/edit";
    }

    @Transactional
    @RequestMapping(value = "/editCity", method = RequestMethod.POST)
    public String editUser(@ModelAttribute("editProfileCityForm") @Valid EditProfileCityDto editProfileCityDto, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".editProfileCityForm", result);

            redirectAttributes.addFlashAttribute("editProfileCityForm", editProfileCityDto);

            // Redirect back to the form
            return "redirect:/user/editCity";
        }

        User user = cityService.addIfNotExistSetIfExistsAtUserEdit(editProfileCityDto);

        userService.edit(user);

        redirectAttributes.addFlashAttribute("flash", new FlashMessage("City updated successfully", FlashMessage.Status.SUCCESS));

        return "redirect:user/profile";
    }

}
