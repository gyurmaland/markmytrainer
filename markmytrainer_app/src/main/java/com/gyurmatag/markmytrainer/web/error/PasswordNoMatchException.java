package com.gyurmatag.markmytrainer.web.error;

public class PasswordNoMatchException extends RuntimeException {
    private static final long serialVersionUID = 5861310537366287163L;

    public PasswordNoMatchException() {
        super();
    }

    public PasswordNoMatchException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public PasswordNoMatchException(final String message) {
        super(message);
    }

    public PasswordNoMatchException(final Throwable cause) {
        super(cause);
    }
}