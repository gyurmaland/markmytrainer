package com.gyurmatag.markmytrainer.web.error;

public final class OneReviewAlreadyExistByTheCurrentUserException extends RuntimeException {
    private static final long serialVersionUID = 5861310537366287163L;

    public OneReviewAlreadyExistByTheCurrentUserException() {
        super();
    }

    public OneReviewAlreadyExistByTheCurrentUserException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public OneReviewAlreadyExistByTheCurrentUserException(final String message) {
        super(message);
    }

    public OneReviewAlreadyExistByTheCurrentUserException(final Throwable cause) {
        super(cause);
    }
}
