package com.gyurmatag.markmytrainer.web.controller;

import com.gyurmatag.markmytrainer.validation.FieldMatch;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public abstract class BaseController {
    protected static final int BUTTONS_TO_SHOW = 5;
    protected static final int INITIAL_PAGE = 0;
    protected static final int INITIAL_PAGE_SIZE = 12;
    protected String searchName;
    protected int evalPageSize;
    protected int evalPage;
    // Support the 'Back' button stuff
    protected int formRealodTimes = 0;
    protected String reloadedPage = null;
    protected int backTimes = 0;
    protected String basicBack = null;

    // Paging stuff
    public BaseController() {
        searchName = null;
        evalPageSize = 0;
        evalPage = 0;
    }

    protected int calculateBackTimes(HttpServletRequest request, boolean matches, String urlFragment) {
        reloadedPage = getPreviousPageByRequest(request).orElse("/");
        backTimes = 0;
        basicBack = null;

        if (matches == true) {
            if (reloadedPage.matches(urlFragment)) {
                formRealodTimes++;
            } else {
                formRealodTimes = 0;
            }
        } else {
            if (reloadedPage.contains(urlFragment)) {
                formRealodTimes++;
            } else {
                formRealodTimes = 0;
            }
        }


        backTimes = -1 - formRealodTimes;

        return backTimes;
    }

    protected Optional<String> getPreviousPageByRequest(HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader("Referer"));
    }

    protected List<String> orderByOptionsForTrainer() {
        List<String> orderByOptions = Arrays.asList("Legnépszerűbb", "Legutáltabb", "Legtöbb értékelés");

        return orderByOptions;
    }

    protected List<String> orderByOptionsForGym() {
        List<String> orderByOptions = Arrays.asList("Legtöbb edző", "Legjobb értékelések", "Legrosszabb értékelések");

        return orderByOptions;
    }

}
