package com.gyurmatag.markmytrainer.repository;

import com.gyurmatag.markmytrainer.model.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ReviewRepository extends PagingAndSortingRepository<Review, Long> {
    List<Review> findAll();

    List<Review> findByCreatedBy(String username);

    List<Review> findReviewsByTrainerId(Long id);

    Review findById(Long id);

    Review save(Review review);

    void deleteById(Long id);

    Page<Review> findAllPageableByTrainerIdOrderByRatingDesc(Long id, Pageable pageable);

    Page<Review> findAllPageableByTrainerIdOrderByRatingAsc(Long id, Pageable pageable);

    Page<Review> findAllPageableByCreatedBy(String username, Pageable pageable);
}
