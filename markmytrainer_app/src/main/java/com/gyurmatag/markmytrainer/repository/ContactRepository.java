package com.gyurmatag.markmytrainer.repository;

import com.gyurmatag.markmytrainer.model.City;
import com.gyurmatag.markmytrainer.model.Contact;
import org.springframework.data.repository.CrudRepository;

public interface ContactRepository extends CrudRepository<City, Long> {

    Contact save(Contact contact);
}
