package com.gyurmatag.markmytrainer.repository;

import com.gyurmatag.markmytrainer.model.Gym;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface GymRepository extends JpaRepository<Gym, Long>, QueryDslPredicateExecutor<Gym> {

    @Query("select g, avg(gtr.rating) as ratingAvg, count(gtr.rating) as ratingCount from Gym g " +
            "left join g.trainers as gt left join gt.reviews as gtr " +
            "where (:gymName='' or lower(g.name) like concat('%', lower(:gymName), '%') ) " +
            "and (:cityName='' or lower(g.city.name) like concat('%', lower(:cityName), '%') ) "+
            "group by g.id order by ratingAvg desc nulls last")
    Page<Gym> findAllAdvancedSortedByAvgTrainerRatingDesc(@Param("gymName") String gymName,
                                                       @Param("cityName") String cityName,
                                                       Pageable pageable);

    @Query("select g, avg(gtr.rating) as ratingAvg, count(gtr.rating) as ratingCount from Gym g " +
            "left join g.trainers as gt left join gt.reviews as gtr " +
            "where (:gymName='' or lower(g.name) like concat('%', lower(:gymName), '%') ) " +
            "and (:cityName='' or lower(g.city.name) like concat('%', lower(:cityName), '%') ) "+
            "group by g.id order by ratingAvg asc nulls last")
    Page<Gym> findAllAdvancedSortedByAvgTrainerRatingAsc(@Param("gymName") String gymName,
                                                          @Param("cityName") String cityName,
                                                          Pageable pageable);

    List<Gym> findByCityId(Long id);

    Gym findById(Long id);

    Gym save(Gym gym);

    @Transactional
    @Modifying
    @Query("update Gym g set g.picURL = :picURL where g.id = :id")
    void updatePicURL(@Param("picURL") String picURL, @Param("id") Long id);

    @Query("select count(gtr.rating) from Gym g left join g.trainers as gt left join gt.reviews as gtr where g.id=?1 group by g.id")
    int getTrainerReviewCount(Long id);

    @Query("select avg(gtr.rating) from Gym g left join g.trainers as gt left join gt.reviews as gtr where g.id=?1 group by g.id")
    Double getTrainerScoreAvg(Long id);

    @Query("select count(gt.id) from Gym g left join g.trainers as gt where g.id=?1")
    int getTrainerCount(Long id);

    // autocomplete-hoz
    List<Gym> findTop5ByNameContainsIgnoreCase(String substr);

}