package com.gyurmatag.markmytrainer.repository;

import com.gyurmatag.markmytrainer.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends CrudRepository<User, Long> {
    User findById(Long id);

    User findByUsername(String username);

    User findByEmail(String email);

    User findByActivation(String code);

    @Modifying
    @Query("update User u set u.password = :password where u.id = :id")
    void updatePassword(@Param("password") String password, @Param("id") Long id);

    User save(User user);
}
