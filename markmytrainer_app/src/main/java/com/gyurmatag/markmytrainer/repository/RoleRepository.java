package com.gyurmatag.markmytrainer.repository;

import com.gyurmatag.markmytrainer.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
    Role findById(Long id);

    Role findByName(String role);
}
