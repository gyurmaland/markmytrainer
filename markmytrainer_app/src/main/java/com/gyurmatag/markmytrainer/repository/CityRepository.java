package com.gyurmatag.markmytrainer.repository;

import com.gyurmatag.markmytrainer.model.City;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CityRepository extends CrudRepository<City, Long> {
    List<City> findAll();

    City findById(Long id);

    City findByNameIgnoreCase(String name);

    List<City> findTop5ByNameContainsIgnoreCase(String substr);

}
