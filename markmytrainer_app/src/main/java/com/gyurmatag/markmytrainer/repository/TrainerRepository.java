package com.gyurmatag.markmytrainer.repository;

import com.gyurmatag.markmytrainer.model.Trainer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;

public interface TrainerRepository extends JpaRepository<Trainer, Long>, QueryDslPredicateExecutor<Trainer> {

    @Query("select t, avg(r.rating) as ratingAvg, count(r.rating) as ratingCount from Trainer t " +
            "left join t.reviews as r " +
            "where (:trainerName='' or lower(t.name) like concat('%', lower(:trainerName), '%') ) " +
            "and (:gymName='' or lower(t.gym.name) like concat('%', lower(:gymName), '%') ) " +
            "and (:cityName='' or lower(t.gym.city.name) like concat('%', lower(:cityName), '%') ) "+
            "group by t.id order by ratingAvg desc nulls last")
    Page<Trainer> findAllAdvancedSortedByAvgRatingDesc(@Param("trainerName") String trainerName,
                                                       @Param("gymName") String gymName,
                                                       @Param("cityName") String cityName,
                                                       Pageable pageable);

    @Query("select t, avg(r.rating) as ratingAvg, count(r.rating) as ratingCount from Trainer t " +
            "left join t.reviews as r " +
            "where (:trainerName='' or lower(t.name) like concat('%', lower(:trainerName), '%') ) " +
            "and (:gymName='' or lower(t.gym.name) like concat('%', lower(:gymName), '%') ) " +
            "and (:cityName='' or lower(t.gym.city.name) like concat('%', lower(:cityName), '%') ) "+
            "group by t.id order by ratingAvg asc nulls last")
    Page<Trainer> findAllAdvancedSortedByAvgRatingAsc(@Param("trainerName") String trainerName,
                                                       @Param("gymName") String gymName,
                                                       @Param("cityName") String cityName,
                                                       Pageable pageable);

    @Query("select t, avg(r.rating) as ratingCount, count(r.rating) as ratingCount from Trainer t " +
            "left join t.reviews as r " +
            "where (:trainerName='' or lower(t.name) like concat('%', lower(:trainerName), '%') ) " +
            "and (:gymName='' or lower(t.gym.name) like concat('%', lower(:gymName), '%') ) " +
            "and (:cityName='' or lower(t.gym.city.name) like concat('%', lower(:cityName), '%') ) "+
            "group by t.id order by ratingCount desc nulls last")
    Page<Trainer> findAllAdvancedSortedByRatingNumberDesc(@Param("trainerName") String trainerName,
                                                      @Param("gymName") String gymName,
                                                      @Param("cityName") String cityName,
                                                      Pageable pageable);


    Trainer findById(Long id);

    // autocomplete-hoz
    List<Trainer> findTop5ByNameContainsIgnoreCase(String substr);

    List<Trainer> findTop5ByNameContainsIgnoreCaseAndGymId(String substr, Long id);

    @Query("select count(rating) from Review where trainer_id = ?1")
    int getCountRatingById(Long id);

    @Query("select avg(rating) from Review where trainer_id = ?1")
    double getAverageRatingById(Long id);

    // edző listázók értékelések átlaga alapján
    @Query("select t from Trainer t left join t.reviews as r group by t.id order by avg(r.rating) desc")
    Page<Trainer> getTrainersSortedByAvgRatingDesc(Pageable pageable);

    @Query("select t from Trainer t left join t.reviews as r group by t.id order by avg(r.rating) asc")
    Page<Trainer> getTrainersSortedByAvgRatingAsc(Pageable pageable);

    Trainer save(Trainer trainer);

    @Modifying
    @Query("update Trainer t set t.picURL = :picURL where t.id = :id")
    void updatePicURL(@Param("picURL") String picURL, @Param("id") Long id);

    Page<Trainer> findTrainersByNameContainingIgnoreCase(Pageable pageable, String name);

    Page<Trainer> findAllPageableByGymId(Long id, Pageable pageable);

    Page<Trainer> findTrainersByGymIdAndNameContainingIgnoreCase(Pageable pageable, Long id, String name);

    // edző listázók értékelése átlaga alapján az egyes konditermekben
    @Query("select t, avg(r.rating) as ratingAvg, count(r.rating) as ratingCount from Trainer t left join t.reviews as r where gym_id =?1 group by t.id order by ratingAvg desc")
    Page<Trainer> findAllPageableByGymIdSortedByAvgRatingDesc(Long id, Pageable pageable);

}
