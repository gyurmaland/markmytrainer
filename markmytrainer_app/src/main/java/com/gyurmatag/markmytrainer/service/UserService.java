package com.gyurmatag.markmytrainer.service;

import com.gyurmatag.markmytrainer.model.PasswordResetToken;
import com.gyurmatag.markmytrainer.model.User;
import com.gyurmatag.markmytrainer.web.dto.EditProfileCityDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User findById(Long id);

    User findByUsername(String username);

    EditProfileCityDto findByUserNameForEdit(String username);

    User findByEmail(String email);

    User findByActivation(String code);

    void userActivation(String code);

    void updatePassword(String password, Long userId);

    void changePassword(String oldPassword, String newPassword, Long userId);

    void save(User user);

    void edit(User user);

    void sendMailWithPasswordResetLink(User user, PasswordResetToken passwordResetToken);

    String generateKey();
}
