package com.gyurmatag.markmytrainer.service;

import com.gyurmatag.markmytrainer.model.Contact;
import com.gyurmatag.markmytrainer.repository.ContactRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class ContactServiceImpl implements ContactService {

    private final Log log = LogFactory.getLog(this.getClass());

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String MESSAGE_FROM_AND_TO;

    @Override
    public void save(Contact contact) {
        contactRepository.save(contact);
    }

    @Override
    public void sendMailToSupport(String name, String email, String message) {
        SimpleMailMessage simpleMailMessage = null;

        try {
            simpleMailMessage = new SimpleMailMessage();
            simpleMailMessage.setFrom(MESSAGE_FROM_AND_TO);
            simpleMailMessage.setTo(MESSAGE_FROM_AND_TO);
            simpleMailMessage.setSubject("Support msg from " + email);
            simpleMailMessage.setText(message);
            javaMailSender.send(simpleMailMessage);

        } catch (Exception e) {
            log.error("Error sending support email" + e);
        }
    }
}
