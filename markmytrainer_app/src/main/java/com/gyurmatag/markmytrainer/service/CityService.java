package com.gyurmatag.markmytrainer.service;

import com.gyurmatag.markmytrainer.model.City;
import com.gyurmatag.markmytrainer.model.Gym;
import com.gyurmatag.markmytrainer.model.User;
import com.gyurmatag.markmytrainer.web.dto.AutocompleteForAllCitiesDto;
import com.gyurmatag.markmytrainer.web.dto.EditProfileCityDto;
import com.gyurmatag.markmytrainer.web.dto.GymAddDto;
import com.gyurmatag.markmytrainer.web.dto.SignUpDto;

import java.util.List;

public interface CityService {
    List<City> findAll();

    City findById(Long id);

    City findByNameIgnoreCase(String name);

    Boolean isCityExists(String cityName);

    //TODO: kérdéses!!??
    Gym addIfNotExistSetIfExistsAtGyms(GymAddDto gymAddDto);

    User addIfNotExistSetIfExistsAtUsers(SignUpDto signUpDto);

    User addIfNotExistSetIfExistsAtUserEdit(EditProfileCityDto editProfileCityDto);

    void save(City city);

    // autocomplete-hoz
    List<AutocompleteForAllCitiesDto> findTop5ByNameContainsIgnoreCase(String substr);
}
