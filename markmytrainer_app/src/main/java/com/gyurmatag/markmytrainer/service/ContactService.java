package com.gyurmatag.markmytrainer.service;

import com.gyurmatag.markmytrainer.model.Contact;

public interface ContactService {
    void save(Contact contact);

    void sendMailToSupport(String name, String email, String message);
}
