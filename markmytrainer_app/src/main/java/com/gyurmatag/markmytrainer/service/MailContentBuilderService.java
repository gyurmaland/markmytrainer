package com.gyurmatag.markmytrainer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailContentBuilderService {
    private TemplateEngine templateEngine;

    @Autowired
    public MailContentBuilderService(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public String buildVerificationTemplate(String link) {
        Context context = new Context();
        context.setVariable("link", link);
        return templateEngine.process("email/verificationMailTemplate", context);
    }

    public String buildPwResetTemplate(String link) {
        Context context = new Context();
        context.setVariable("link", link);
        return templateEngine.process("email/pwResetMailTemplate", context);
    }
}
