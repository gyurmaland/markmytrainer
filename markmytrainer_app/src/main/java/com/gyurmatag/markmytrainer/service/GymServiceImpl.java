package com.gyurmatag.markmytrainer.service;

import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.gyurmatag.markmytrainer.cloudinary.CloudinarySingleton;
import com.gyurmatag.markmytrainer.mapper.ObjectMapperUtils;
import com.gyurmatag.markmytrainer.model.Gym;
import com.gyurmatag.markmytrainer.predicate.GymPredicate;
import com.gyurmatag.markmytrainer.repository.GymRepository;
import com.gyurmatag.markmytrainer.web.dto.AutocompleteForAllGymsDto;
import com.gyurmatag.markmytrainer.web.error.GymNotFoundException;
import com.gyurmatag.markmytrainer.web.error.InvalidFileExtensionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class GymServiceImpl implements GymService {
    @Autowired
    private GymRepository gymRepository;

    @Override
    public Page<Gym> findAllAdvancedSortedByAvgTrainerRatingDesc(Optional<String> gymName, Optional<String> cityName, Pageable pageable) {
        Page<Gym> allGymsAdvanced = gymRepository.findAllAdvancedSortedByAvgTrainerRatingDesc(gymName.orElse("").trim(), cityName.orElse("").trim(), pageable);

        return allGymsAdvanced;
    }

    @Override
    public Page<Gym> findAllAdvancedSortedByAvgTrainerRatingAsc(Optional<String> gymName, Optional<String> cityName, Pageable pageable) {
        Page<Gym> allGymsAdvanced = gymRepository.findAllAdvancedSortedByAvgTrainerRatingAsc(gymName.orElse("").trim(), cityName.orElse("").trim(), pageable);

        return allGymsAdvanced;
    }

    @Override
    public List<Gym> findAll() {
        return gymRepository.findAll();
    }

    @Override
    public Page<Gym> findAllAdvanced(Optional<String> gymName, Optional<String> cityName, QPageRequest qPageRequest) {

        Page<Gym> allGymAdvanced = gymRepository.findAll(GymPredicate.advancedGymSearch(gymName, cityName), qPageRequest);

        return allGymAdvanced;
    }

    @Override
    public List<Gym> findByCityId(Long id) {
        return gymRepository.findByCityId(id);
    }

    @Override
    public Gym findById(Long id) {
        Gym gym = gymRepository.findById(id);

        if(gym == null) {
            throw new GymNotFoundException("Nincs ilyen edzőterem, vagy már törölték :(");
        }

        return gym;
    }

    @Override
    public File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException {
        File convFile = new File(multipart.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(multipart.getBytes());
        fos.close();
        return convFile;
    }

    @Override
    public Long save(Gym gym, MultipartFile file) throws InvalidFileExtensionException {
        gymRepository.save(gym);

        String filename = Long.toString(gym.getId());

        Map result = null;

        if (!file.isEmpty()) {
            try {
                result = CloudinarySingleton.getCloudinary().uploader().upload(multipartToFile(file), ObjectUtils.asMap(
                        "public_id", "gymImgs/" + filename, "transformation", new Transformation().gravity("auto").height(400).width(400).crop("fill")));
            } catch (IOException e) {
                e.printStackTrace();
            }
            updatePicURL(result.get("url").toString(), gym.getId());
        }

        return gym.getId();

    }

    @Override
    public void updatePicURL(String picURL, Long id) {
        gymRepository.updatePicURL(picURL, id);
    }

    @Override
    public int getTrainerReviewCount(Long id) {
        return gymRepository.getTrainerReviewCount(id);
    }

    @Override
    public Double getTrainerScoreAvg(Long id) {
        return gymRepository.getTrainerScoreAvg(id);
    }

    @Override
    public int getTrainerCount(Long id) {
        return gymRepository.getTrainerCount(id);
    }

    @Override
    public List<AutocompleteForAllGymsDto> findTop5ByNameContainsIgnoreCase(String substr) {
        List<Gym> gymsList = gymRepository.findTop5ByNameContainsIgnoreCase(substr);

        List<AutocompleteForAllGymsDto> gymListForAutocomplete = ObjectMapperUtils.mapAll(gymsList, AutocompleteForAllGymsDto.class);


        return gymListForAutocomplete;
    }

}
