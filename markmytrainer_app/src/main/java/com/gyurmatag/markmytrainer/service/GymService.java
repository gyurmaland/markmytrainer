package com.gyurmatag.markmytrainer.service;

import com.gyurmatag.markmytrainer.model.Gym;
import com.gyurmatag.markmytrainer.web.dto.AutocompleteForAllGymsDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface GymService {
    Page<Gym> findAllAdvancedSortedByAvgTrainerRatingDesc( Optional<String> gymName,
                                                          Optional<String> cityName,
                                                          Pageable pageable);

    Page<Gym> findAllAdvancedSortedByAvgTrainerRatingAsc( Optional<String> gymName,
                                                           Optional<String> cityName,
                                                           Pageable pageable);

    List<Gym> findAll();

    Page<Gym> findAllAdvanced(Optional<String> gymName, Optional<String> cityName, QPageRequest qPageRequest);

    List<Gym> findByCityId(Long id);

    Gym findById(Long id);

    @Secured("ROLE_USER")
    File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException;

    @Secured("ROLE_USER")
    Long save(Gym gym, MultipartFile file);

    @Secured("ROLE_USER")
    void updatePicURL(String picURL, Long id);

    int getTrainerReviewCount(Long id);

    Double getTrainerScoreAvg(Long id);

    int getTrainerCount(Long id);

    // autocomplete-hoz
    List<AutocompleteForAllGymsDto> findTop5ByNameContainsIgnoreCase(String substr);


}
