package com.gyurmatag.markmytrainer.service;


import com.gyurmatag.markmytrainer.model.Trainer;
import com.gyurmatag.markmytrainer.web.dto.AutocompleteForAllTrainersDto;
import com.gyurmatag.markmytrainer.web.dto.TrainerAddDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface TrainerService {
    List<Trainer> findAll();

    Page<Trainer> findAllAdvancedSortedByAvgRatingDesc(Optional<String> trainerName, Optional<String> gymName, Optional<String> cityName, Pageable pageable);

    Page<Trainer> findAllAdvancedSortedByAvgRatingAsc(Optional<String> trainerName, Optional<String> gymName, Optional<String> cityName, Pageable pageable);

    Page<Trainer> findAllAdvancedSortedByRatingNumberDesc(Optional<String> trainerName, Optional<String> gymName, Optional<String> cityName, Pageable pageable);

    Trainer findById(Long id);

    List<AutocompleteForAllTrainersDto> findTop5ByNameContainsIgnoreCase(String substr);

    List<Trainer> findTop5ByNameContainsIgnoreCaseAndGymId(String substr, Long id);

    int getCountRatingById(Long id);

    double getAverageRatingById(Long id);

    Page<Trainer> getTrainersSortedByAvgRatingAsc(Pageable pageable);

    Page<Trainer> getTrainersSortedByAvgRatingDesc(Pageable pageable);

    @Secured("ROLE_USER")
    File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException;

    @Secured("ROLE_USER")
    Long save(TrainerAddDto trainerAddDto, MultipartFile file);

    @Secured("ROLE_USER")
    void updatePicURL(String picURL, Long id);

    Page<Trainer> findAllPageable(Pageable pageable);

    Page<Trainer> findAllPageableByGymId(Long id, Pageable pageable);

    Page<Trainer> findTrainersByGymIdAndNameContainingIgnoreCase(Pageable pageable, Long id, String name);

    Page<Trainer> findTrainersByNameContainingIgnoreCase(Pageable pageable, String name);

    Page<Trainer> findAllPageableByGymIdSortedByAvgRatingDesc(Long id, Pageable pageable);
}
