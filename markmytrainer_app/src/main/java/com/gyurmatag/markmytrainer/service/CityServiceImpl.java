package com.gyurmatag.markmytrainer.service;

import com.gyurmatag.markmytrainer.mapper.ObjectMapperUtils;
import com.gyurmatag.markmytrainer.model.City;
import com.gyurmatag.markmytrainer.model.Gym;
import com.gyurmatag.markmytrainer.model.User;
import com.gyurmatag.markmytrainer.repository.CityRepository;
import com.gyurmatag.markmytrainer.web.dto.AutocompleteForAllCitiesDto;
import com.gyurmatag.markmytrainer.web.dto.EditProfileCityDto;
import com.gyurmatag.markmytrainer.web.dto.GymAddDto;
import com.gyurmatag.markmytrainer.web.dto.SignUpDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {
    @Autowired
    private CityRepository cityRepository;

    @Override
    public List<City> findAll() {
        return cityRepository.findAll();
    }

    @Override
    public City findById(Long id) {
        return cityRepository.findById(id);
    }

    @Override
    public City findByNameIgnoreCase(String name) {
        return cityRepository.findByNameIgnoreCase(name);
    }

    @Override
    public Boolean isCityExists(String cityName) {
        List<City> allCitites = cityRepository.findAll();
        boolean isCityExist = false;
        int i = 0;
        while (isCityExist == false && i < allCitites.size()) {
            if (cityName.equals(allCitites.get(i).getName())) {
                isCityExist = true;
            }
            i++;
        }

        return isCityExist;
    }

    @Override
    public Gym addIfNotExistSetIfExistsAtGyms(GymAddDto gymAddDto) {
        Gym gym = ObjectMapperUtils.map(gymAddDto, Gym.class);

        City city = gym.getCity();

        if (isCityExists(gymAddDto.getCityName()) == true) {
            gym.setCity(cityRepository.findByNameIgnoreCase(city.getName()));
        } else {
            city.setName(gymAddDto.getCityName());
            cityRepository.save(city);
            gym.setCity(cityRepository.findByNameIgnoreCase(city.getName()));
        }

        return gym;

    }

    @Override
    public User addIfNotExistSetIfExistsAtUsers(SignUpDto signUpDto) {
        User user = ObjectMapperUtils.map(signUpDto, User.class);

        City city = user.getCity();

        if (isCityExists(signUpDto.getCityName()) == true) {
            user.setCity(cityRepository.findByNameIgnoreCase(city.getName()));
        } else {
            city.setName(signUpDto.getCityName());
            cityRepository.save(city);
            user.setCity(cityRepository.findByNameIgnoreCase(city.getName()));
        }


        return user;
    }

    @Override
    public User addIfNotExistSetIfExistsAtUserEdit(EditProfileCityDto editProfileCityDto) {

        User user = ObjectMapperUtils.map(editProfileCityDto, User.class);

        City city = user.getCity();

        if (isCityExists(editProfileCityDto.getCityName()) == true) {
            user.setCity(cityRepository.findByNameIgnoreCase(city.getName()));
        } else {
            city.setName(editProfileCityDto.getCityName());
            cityRepository.save(city);
            user.setCity(cityRepository.findByNameIgnoreCase(city.getName()));
        }

        return user;
    }

    @Override
    public void save(City city) {
        cityRepository.save(city);
    }

    @Override
    public List<AutocompleteForAllCitiesDto> findTop5ByNameContainsIgnoreCase(String substr) {
        List<City> cityList = cityRepository.findTop5ByNameContainsIgnoreCase(substr);

        List<AutocompleteForAllCitiesDto> cityListForAutocomplete = ObjectMapperUtils.mapAll(cityList, AutocompleteForAllCitiesDto.class);

        return cityListForAutocomplete;
    }
}
