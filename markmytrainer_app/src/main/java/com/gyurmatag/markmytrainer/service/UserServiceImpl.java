package com.gyurmatag.markmytrainer.service;

import com.gyurmatag.markmytrainer.mapper.ObjectMapperUtils;
import com.gyurmatag.markmytrainer.model.PasswordResetToken;
import com.gyurmatag.markmytrainer.model.User;
import com.gyurmatag.markmytrainer.repository.RoleRepository;
import com.gyurmatag.markmytrainer.repository.UserRepository;
import com.gyurmatag.markmytrainer.web.dto.EditProfileCityDto;
import com.gyurmatag.markmytrainer.web.error.BadActivationCodeException;
import com.gyurmatag.markmytrainer.web.error.BadOldPasswordException;
import com.gyurmatag.markmytrainer.web.error.UserEmailAlreadyExistException;
import com.gyurmatag.markmytrainer.web.error.UsernameAlreadyExistException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class UserServiceImpl implements UserService {
    private final Log log = LogFactory.getLog(this.getClass());
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Value("${spring.mail.username}")
    private String MESSAGE_FROM;

    @Value("${spring.mail.activation.webappurl}")
    private String WEB_APP_URL_FOR_ACTIVATION;

    @Autowired
    private JavaMailSender javaMailSender;

    private boolean emailExist(final String email) {
        return userRepository.findByEmail(email) != null;
    }

    private boolean passwordsMatch(User user) {
        if (passwordEncoder.matches(user.getConfirmPassword(), user.getPassword())) {
            log.info("match!!");
            return true;
        } else {
            log.info("no match!");
            return false;
        }
    }

    private boolean usnernameExist(final String username) {
        return userRepository.findByUsername(username) != null;
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public EditProfileCityDto findByUserNameForEdit(String username) {
        User user = userRepository.findByUsername(username);
        EditProfileCityDto editProfileCityDto = ObjectMapperUtils.map(user, EditProfileCityDto.class);
        return editProfileCityDto;
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User findByActivation(String code) {
        User user = userRepository.findByActivation(code);
        if (user == null) {
            throw new BadActivationCodeException("Érvénytelen aktiváló kóód");
        }
        return user;
    }

    @Override
    public void userActivation(String code) {
        User user = userRepository.findByActivation(code);
        if (user == null) {
            throw new BadActivationCodeException("Érvénytelen aktiváló kod");
        }
        user.setEnabled(true);
        user.setActivation("");
        userRepository.save(user);

    }

    @Override
    public void updatePassword(String password, Long userId) {
        String encodedPassword = passwordEncoder.encode(password);
        userRepository.updatePassword(encodedPassword, userId);
    }

    @Override
    public void changePassword(String oldPassword, String newPassword, Long userId) {
        if (!passwordEncoder.matches(oldPassword, findById(userId).getPassword())) {
            throw new BadOldPasswordException("Rossz régi jelszó");
        }
        String encodedPassword = passwordEncoder.encode(newPassword);
        userRepository.updatePassword(encodedPassword, userId);
    }

    @Override
    public void save(User user) {
        if (emailExist(user.getEmail())) {
            throw new UserEmailAlreadyExistException("Ezzel az e-mail címmel már van egy felhasználó: " + user.getEmail());
        }
        if (usnernameExist(user.getUsername())) {
            throw new UsernameAlreadyExistException("Ezzel a felhasználónévvel már van egy felhasználó: " + user.getUsername());
        }

        String activationKey = generateKey();


        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEnabled(false);
        user.setRole(roleRepository.findByName("ROLE_USER"));
        user.setActivation(activationKey);

        userRepository.save(user);
    }

    @Override
    public void edit(User user) {
        Long userId = user.getId();
        user.setUsername(findById(userId).getUsername());
        user.setEmail(findById(userId).getEmail());
        user.setPassword(findById(userId).getPassword());
        user.setEnabled(true);
        user.setRole(roleRepository.findByName("ROLE_USER"));
        user.setActivation("");

        userRepository.save(user);
    }

    @Override
    public void sendMailWithPasswordResetLink(User user, PasswordResetToken passwordResetToken) {
        SimpleMailMessage message = null;

        try {
            message = new SimpleMailMessage();
            message.setFrom("markmytrainer@gmail.com");
            message.setTo(user.getEmail());
            message.setSubject("Reset password link from MarkMyTrainer!");
            message.setText("Dear " + user.getUsername() + "! \n \nHere is your password reset link!\n \n" +
                    "Click here:\n" + "http://localhost:8080/resetPassword/" + passwordResetToken.getToken());
            //On server: http://markmytrainer.com/resetPassword
            javaMailSender.send(message);

        } catch (Exception e) {
            log.error("Error sending email to the following address: " + user.getEmail() + "  " + e);
        }
    }

    @Override
    public String generateKey() {
        String key = "";
        Random random = new Random();
        char[] word = new char[16];
        for (int j = 0; j < word.length; j++) {
            word[j] = (char) ('a' + random.nextInt(26));
        }
        String toReturn = new String(word);
        log.debug("random code: " + toReturn);
        return new String(word);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // Load the user from the database (throw exception if not found)
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Nincs ilyen felhasználó");
        }

        // Return user object
        return user;
    }
}
