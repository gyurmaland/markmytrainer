package com.gyurmatag.markmytrainer.service;

import com.gyurmatag.markmytrainer.model.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;

import java.util.List;

public interface ReviewService {
    List<Review> findAll();

    @Secured("ROLE_USER")
    List<Review> findByCreatedBy();

    List<Review> findReviewsByTrainerId(Long id);

    Review findById(Long id);

    @Secured("ROLE_USER")
    Review findByIdForEdit(Long id);

    @Secured("ROLE_USER")
    void save(Review review);

    @Secured("ROLE_USER")
    void edit(Review review);

    @Secured("ROLE_USER")
    void deleteById(Long id);

    Page<Review> findAllPageableByTrainerIdOrderByRatingDesc(Long id, Pageable pageable);

    Page<Review> findAllPageableByTrainerIdOrderByRatingAsc(Long id, Pageable pageable);

    @Secured("ROLE_USER")
    Page<Review> findAllPageableByCreatedBy(Pageable pageable);
}
