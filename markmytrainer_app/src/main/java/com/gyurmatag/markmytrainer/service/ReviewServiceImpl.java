package com.gyurmatag.markmytrainer.service;

import com.gyurmatag.markmytrainer.model.Review;
import com.gyurmatag.markmytrainer.model.User;
import com.gyurmatag.markmytrainer.repository.ReviewRepository;
import com.gyurmatag.markmytrainer.web.error.NotCurrentUserReviewException;
import com.gyurmatag.markmytrainer.web.error.OneReviewAlreadyExistByTheCurrentUserException;
import com.gyurmatag.markmytrainer.web.error.ReviewNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {
    @Autowired
    private ReviewRepository reviewRepository;

    private boolean oneReviewAlreadyExistByCurrentUser(final String username, final Long trainerId) {
        List<Review> reviewsByTrainer = reviewRepository.findReviewsByTrainerId(trainerId);

        boolean alreadyExist = false;

        int i = 0;
        while (!alreadyExist && i < reviewsByTrainer.size()) {
            if (reviewsByTrainer.get(i).getCreatedBy().equals(username)) {
                alreadyExist = true;
            }
            i++;
        }

        return alreadyExist;
    }

    @Override
    public List<Review> findAll() {
        return reviewRepository.findAll();
    }

    @Override
    public List<Review> findByCreatedBy() {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return reviewRepository.findByCreatedBy(currentUser.getUsername());
    }

    @Override
    public List<Review> findReviewsByTrainerId(Long id) {
        return reviewRepository.findReviewsByTrainerId(id);
    }

    @Override
    public Review findById(Long id) {
        return reviewRepository.findById(id);
    }

    @Override
    public Review findByIdForEdit(Long id) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Review review = reviewRepository.findById(id);

        if(review == null) {
            throw new ReviewNotFoundException("Nincs ilyen értékelés");
        }

        if (!review.getCreatedBy().equals(currentUser.getUsername())) {
            throw new NotCurrentUserReviewException("Nincs jogosultságod szerkeszteni ezt az értékelést!");
        }

        return review;
    }

    @Override
    public void save(Review review) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (oneReviewAlreadyExistByCurrentUser(currentUser.getUsername(), review.getTrainer().getId())) {
            throw new OneReviewAlreadyExistByTheCurrentUserException("Már van egy értékelés tőled erre az edzőre!");
        }

        reviewRepository.save(review);
    }

    @Override
    public void edit(Review review) {
        reviewRepository.save(review);
    }

    @Override
    public void deleteById(Long id) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Review review = reviewRepository.findById(id);

        if(review == null) {
            throw new ReviewNotFoundException("Nincs ilyen értékelés");
        }

        if (!review.getCreatedBy().equals(currentUser.getUsername())) {
            throw new NotCurrentUserReviewException("Nincs jogod törölni ezt az értékelést, ejnye");
        }

        reviewRepository.deleteById(id);
    }

    @Override
    public Page<Review> findAllPageableByTrainerIdOrderByRatingDesc(Long id, Pageable pageable) {
        return reviewRepository.findAllPageableByTrainerIdOrderByRatingDesc(id, pageable);
    }

    @Override
    public Page<Review> findAllPageableByTrainerIdOrderByRatingAsc(Long id, Pageable pageable) {
        return reviewRepository.findAllPageableByTrainerIdOrderByRatingAsc(id, pageable);
    }

    @Override
    public Page<Review> findAllPageableByCreatedBy(Pageable pageable) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return reviewRepository.findAllPageableByCreatedBy(currentUser.getUsername(), pageable);
    }
}
