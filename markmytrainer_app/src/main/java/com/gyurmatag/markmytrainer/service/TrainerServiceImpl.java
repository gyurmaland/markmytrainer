package com.gyurmatag.markmytrainer.service;

import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.gyurmatag.markmytrainer.cloudinary.CloudinarySingleton;
import com.gyurmatag.markmytrainer.mapper.ObjectMapperUtils;
import com.gyurmatag.markmytrainer.model.Trainer;
import com.gyurmatag.markmytrainer.repository.TrainerRepository;
import com.gyurmatag.markmytrainer.web.dto.AutocompleteForAllTrainersDto;
import com.gyurmatag.markmytrainer.web.dto.TrainerAddDto;
import com.gyurmatag.markmytrainer.web.error.InvalidFileExtensionException;
import com.gyurmatag.markmytrainer.web.error.TrainerNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class TrainerServiceImpl implements TrainerService {
    @Autowired
    private TrainerRepository trainerRepository;

    @Override
    public List<Trainer> findAll() {
        return trainerRepository.findAll();
    }

    @Override
    public Page<Trainer> findAllAdvancedSortedByAvgRatingDesc(Optional<String> trainerName, Optional<String> gymName, Optional<String> cityName, Pageable pageable) {
        Page<Trainer> allTrainersAdvanced = trainerRepository.findAllAdvancedSortedByAvgRatingDesc(trainerName.orElse("").trim(), gymName.orElse("").trim(), cityName.orElse("").trim(), pageable);

        return allTrainersAdvanced;
    }

    @Override
    public Page<Trainer> findAllAdvancedSortedByAvgRatingAsc(Optional<String> trainerName, Optional<String> gymName, Optional<String> cityName, Pageable pageable) {
        Page<Trainer> allTrainersAdvanced = trainerRepository.findAllAdvancedSortedByAvgRatingAsc(trainerName.orElse("").trim(), gymName.orElse("").trim(), cityName.orElse("").trim(), pageable);

        return allTrainersAdvanced;
    }

    @Override
    public Page<Trainer> findAllAdvancedSortedByRatingNumberDesc(Optional<String> trainerName, Optional<String> gymName, Optional<String> cityName, Pageable pageable) {
        Page<Trainer> allTrainersAdvanced = trainerRepository.findAllAdvancedSortedByRatingNumberDesc(trainerName.orElse("").trim(), gymName.orElse("").trim(), cityName.orElse("").trim(), pageable);

        return allTrainersAdvanced;
    }

    @Override
    public Trainer findById(Long id) {
        Trainer trainer = trainerRepository.findById(id);

        if(trainer == null) {
            throw new TrainerNotFoundException("Nincs ilyen edző, vagy már törölték :(");
        }

        return trainer;
    }

    @Override
    public List<AutocompleteForAllTrainersDto> findTop5ByNameContainsIgnoreCase(String substr) {
        List<Trainer> trainerList = trainerRepository.findTop5ByNameContainsIgnoreCase(substr);

        List<AutocompleteForAllTrainersDto> trainerListForAutocomplete = ObjectMapperUtils.mapAll(trainerList, AutocompleteForAllTrainersDto.class);

        return trainerListForAutocomplete;
    }

    @Override
    public List<Trainer> findTop5ByNameContainsIgnoreCaseAndGymId(String substr, Long id) {
        return trainerRepository.findTop5ByNameContainsIgnoreCaseAndGymId(substr, id);
    }

    @Override
    public int getCountRatingById(Long id) {
        return trainerRepository.getCountRatingById(id);
    }

    @Override
    public double getAverageRatingById(Long id) {
        return trainerRepository.getAverageRatingById(id);
    }

    @Override
    public Page<Trainer> getTrainersSortedByAvgRatingAsc(Pageable pageable) {
        return trainerRepository.getTrainersSortedByAvgRatingAsc(pageable);
    }

    @Override
    public Page<Trainer> getTrainersSortedByAvgRatingDesc(Pageable pageable) {
        return trainerRepository.getTrainersSortedByAvgRatingDesc(pageable);
    }

    @Override
    public File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException {
        File convFile = new File(multipart.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(multipart.getBytes());
        fos.close();
        return convFile;
    }


    // Ez az elmentett edző id-ját adja vissza, hogy tudjunk oda redirectelni
    @Override
    public Long save(TrainerAddDto trainerAddDto, MultipartFile file) throws InvalidFileExtensionException {
        Trainer trainer = ObjectMapperUtils.map(trainerAddDto, Trainer.class);

        trainerRepository.save(trainer);

        String filename = Long.toString(trainer.getId());

        Map result = null;

        if (!file.isEmpty()) {
            try {
                result = CloudinarySingleton.getCloudinary().uploader().upload(multipartToFile(file), ObjectUtils.asMap(
                        "public_id", "trainerImgs/" + filename, "transformation", new Transformation().width(500).height(500).gravity("face").radius("max").crop("crop").chain().crop("scale")));
            } catch (IOException e) {
                e.printStackTrace();
            }
            updatePicURL(result.get("url").toString(), trainer.getId());
        }

        return trainer.getId();
    }

    @Override
    public void updatePicURL(String picURL, Long id) {
        trainerRepository.updatePicURL(picURL, id);
    }

    @Override
    public Page<Trainer> findAllPageable(Pageable pageable) {
        return trainerRepository.findAll(pageable);
    }

    @Override
    public Page<Trainer> findAllPageableByGymId(Long id, Pageable pageable) {
        return trainerRepository.findAllPageableByGymId(id, pageable);
    }

    @Override
    public Page<Trainer> findTrainersByGymIdAndNameContainingIgnoreCase(Pageable pageable, Long id, String name) {
        return trainerRepository.findTrainersByGymIdAndNameContainingIgnoreCase(pageable, id, name);
    }

    @Override
    public Page<Trainer> findTrainersByNameContainingIgnoreCase(Pageable pageable, String name) {
        return trainerRepository.findTrainersByNameContainingIgnoreCase(pageable, name);
    }

    @Override
    public Page<Trainer> findAllPageableByGymIdSortedByAvgRatingDesc(Long id, Pageable pageable) {
        return trainerRepository.findAllPageableByGymIdSortedByAvgRatingDesc(id, pageable);
    }
}
