package com.gyurmatag.markmytrainer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
public class MailClientService {
    private JavaMailSender mailSender;

    @Autowired
    MailContentBuilderService mailContentBuilderService;

    @Autowired
    public MailClientService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void prepareAndSendVerificationEmail(String recipient, String subject, String link) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);

            String content = mailContentBuilderService.buildVerificationTemplate(link);
            messageHelper.setText(content, true);

            messageHelper.setFrom("markmytrainer@markmytrainer.com");
            messageHelper.setTo(recipient);
            messageHelper.setSubject(subject);
            //messageHelper.setText(message);
        };
        try {
            mailSender.send(messagePreparator);
        } catch (MailException e) {
            // runtime exception; compiler will not force you to handle it
        }
    }

    public void prepareAndSendPwResetEmail(String recipient, String subject, String link) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);

            String content = mailContentBuilderService.buildPwResetTemplate(link);
            messageHelper.setText(content, true);

            messageHelper.setFrom("markmytrainer@markmytrainer.com");
            messageHelper.setTo(recipient);
            messageHelper.setSubject(subject);
            //messageHelper.setText(message);
        };
        try {
            mailSender.send(messagePreparator);
        } catch (MailException e) {
            // runtime exception; compiler will not force you to handle it
        }
    }
}
