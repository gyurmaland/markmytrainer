package com.gyurmatag.markmytrainer.service;

import com.gyurmatag.markmytrainer.model.Role;

public interface RoleService {
    Role findById(Long id);

    Role findByName(String name);
}
