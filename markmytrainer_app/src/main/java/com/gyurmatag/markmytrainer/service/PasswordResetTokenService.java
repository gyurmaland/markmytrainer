package com.gyurmatag.markmytrainer.service;

import com.gyurmatag.markmytrainer.model.PasswordResetToken;
import com.gyurmatag.markmytrainer.model.User;

public interface PasswordResetTokenService {
    PasswordResetToken findByToken(String token);

    PasswordResetToken findByUser(User user);

    PasswordResetToken generatePasswordResetTokenForUser(User user);

    void save(PasswordResetToken passwordResetToken);

    void delete(PasswordResetToken passwordResetToken);
}
