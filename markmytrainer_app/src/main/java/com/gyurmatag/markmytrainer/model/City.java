package com.gyurmatag.markmytrainer.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
public class City extends BaseEntity {
    @Column(unique = true)
    @NotNull
    @Size(min = 2, max = 100, message = "{city.name.size}")
    private String name;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "city")
    @JsonManagedReference
    private List<Gym> gyms = new ArrayList<>();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "city")
    @JsonManagedReference
    private List<User> users = new ArrayList<>();

    public City() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Gym> getGyms() {
        return gyms;
    }

    public void setGyms(List<Gym> gyms) {
        this.gyms = gyms;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
