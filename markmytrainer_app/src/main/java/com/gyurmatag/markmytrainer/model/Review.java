package com.gyurmatag.markmytrainer.model;


import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Review extends Auditable<String> {
    @NotNull
    @Min(value = 1, message = "{review.rating.interval}")
    @Max(value = 5, message = "{review.rating.interval}")
    private double rating;
    @Column(length = 1000)
    @NotNull
    @Size(min = 3, max = 1000, message = "{review.reviewText.size}")
    private String reviewText;
    @ManyToOne
    @NotNull
    private Trainer trainer;

    public Review() {
        super();
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }
}
