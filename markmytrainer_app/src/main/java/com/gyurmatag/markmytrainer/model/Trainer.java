package com.gyurmatag.markmytrainer.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Trainer extends Auditable<String> {
    @NotNull
    @Size(min = 3, max = 100, message = "{trainer.name.size}")
    private String name;
    @ManyToOne
    @JoinColumn(name = "gym_id")
    @NotNull(message = "{trainer.gym.exist}")
    @JsonBackReference
    private Gym gym;

    private String picURL;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "trainer")
    private List<Review> reviews = new ArrayList<>();

    public Trainer() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gym getGym() {
        return gym;
    }

    public void setGym(Gym gym) {
        this.gym = gym;
    }

    public String getPicURL() {
        return picURL;
    }

    public void setPicURL(String picURL) {
        this.picURL = picURL;
    }

    public List<Review> getReviews() {
        return reviews;
    }
}
