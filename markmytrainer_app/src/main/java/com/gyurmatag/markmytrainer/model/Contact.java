package com.gyurmatag.markmytrainer.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Contact extends BaseEntity {

    @NotNull
    @Size(min = 2, max = 100)
    private String name;

    @NotEmpty(message = "{user.email.exist}")
    @Email(message = "{user.email.valid}")
    private String email;

    @NotNull
    @Size(min = 5)
    private String message;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
