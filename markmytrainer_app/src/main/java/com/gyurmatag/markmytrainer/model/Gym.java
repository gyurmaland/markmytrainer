package com.gyurmatag.markmytrainer.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Gym extends Auditable<String> implements Comparable<Gym> {
    @NotNull
    @Size(min = 3, max = 100, message = "{gym.name.size}")
    private String name;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "gym")
    private List<Trainer> trainers = new ArrayList<>();
    @ManyToOne
    @JoinColumn(name = "city_id")
    @JsonBackReference
    private City city;

    private String picURL;

    public Gym() {
        super();
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Trainer> getTrainers() {
        return trainers;
    }

    public void setTrainers(List<Trainer> trainers) {
        this.trainers = trainers;
    }

    @Override
    public int compareTo(Gym compareGym) {
        return (int) (super.getId() - compareGym.getId());
    }

    public String getPicURL() {
        return picURL;
    }

    public void setPicURL(String picURL) {
        this.picURL = picURL;
    }
}
