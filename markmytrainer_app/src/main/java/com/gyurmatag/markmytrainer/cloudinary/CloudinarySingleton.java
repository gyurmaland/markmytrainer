package com.gyurmatag.markmytrainer.cloudinary;

import com.cloudinary.utils.ObjectUtils;

public class CloudinarySingleton {
    private static com.cloudinary.Cloudinary cloudinary;

    public static void registerCloudinary(com.cloudinary.Cloudinary cloudinary) {
        CloudinarySingleton.cloudinary = cloudinary;
    }

    public static void deregisterCloudinary() {
        cloudinary = null;
    }

    public static com.cloudinary.Cloudinary getCloudinary() {
        if (cloudinary == null) {
            return CloudinarySingleton.DefaultCloudinaryHolder.INSTANCE;
        }
        return cloudinary;
    }

    private static class DefaultCloudinaryHolder {
        public static final com.cloudinary.Cloudinary INSTANCE = new com.cloudinary.Cloudinary(ObjectUtils.asMap(
                "cloud_name", "gyurmatag",
                "api_key", "375634964289817",
                "api_secret", "bm5urrhLrz3pYrAjSlthYDOxQkQ"));
    }
}
