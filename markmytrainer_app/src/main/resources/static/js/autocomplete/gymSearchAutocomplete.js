$(function () {
    const autocompleteLinkStart = document.location.origin;

    $("#searchGymName").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/autocompleteGymsAll",
                dataType: "json",
                data: {
                    maxRows: 6,
                    searchstr: request.term
                },
                success: function (data) {
                    response($.map(data.gymListAll, function (item) {
                        return {
                            label: '<a style="margin-top: 5px"  class="autocomplete-link" href="' + autocompleteLinkStart + '/gyms/' + item.id + '">' +
                            '<span class="autocomplete-li">' + item.name +
                            '<span class="autocomplete-cityname">' + " (" + item.cityName + ")" + '</span>' + '</span></a>',
                            value: item.name
                        }
                    }));
                }
            });
        },
        minLength: 1,
        html: true,
    });
});