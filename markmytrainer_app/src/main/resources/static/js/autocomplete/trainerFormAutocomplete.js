$(function () {

    $("#inputTrainerAddTrainerCityName").autocomplete({
        select: function (event, ui) {
            var selectedCity = ui.item.label;

            $.getJSON("gymsByCityName", {
                cityName: selectedCity,
                ajax: 'true',
            }, function (data) {
                if (data.length == 0) {
                    console.log('There are no gyms in ' + $('#inputTrainerAddTrainerCityName').val());
                } else {
                    console.log('YASSSS');
                    $('#inputTrainerAddTrainerGymName-error').hide();
                }
                var html = '';
                $.each(data, function (key, val) {
                    html += '<option value="' + val.id + '">'
                        + val.name + '</option>';
                });

                $('.chzn-select').html(html);

                $('.chzn-select').trigger("chosen:updated");

                $( "#inputTrainerAddTrainerCityName" ).trigger( "focus");
                $( "#inputTrainerAddTrainerCityName" ).trigger( "click");
            });
        },
        source: function (request, response) {
            $.ajax({
                url: "/autocompleteCitesAll",
                dataType: "json",
                data: {
                    maxRows: 6,
                    searchstr: request.term
                },
                success: function (data) {
                    response($.map(data.cityList, function (item) {
                        return {
                            label: item.name,
                            value: item.name
                        }
                    }));
                }
            });
        },
        minLength: 1,
    });
});