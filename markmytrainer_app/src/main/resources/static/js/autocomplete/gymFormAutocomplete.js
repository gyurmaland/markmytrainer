$(function () {
    $("#inputGymAddGymCityName").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/autocompleteCitesAll",
                dataType: "json",
                data: {
                    maxRows: 6,
                    searchstr: request.term
                },
                success: function (data) {
                    response($.map(data.cityList, function (item) {
                        return {
                            label: item.name,
                            value: item.name
                        }
                    }));
                }
            });
        },
        minLength: 1,
    });
});