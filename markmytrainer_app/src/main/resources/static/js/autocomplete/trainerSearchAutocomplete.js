$(function () {
    const autocompleteLinkStart = document.location.origin;

    $("#searchTrainerName").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/autocompleteTrainersAll",
                dataType: "json",
                data: {
                    maxRows: 6,
                    searchstr: request.term
                },
                success: function (data) {
                    response($.map(data.trainerListAll, function (item) {
                        return {
                            label: '<a style="margin-top: 5px" class="autocomplete-link" href="' + autocompleteLinkStart + '/trainers/' + item.id + '">' +
                            '<img class="autocomplete-img" src="' + item.picURL + '" alt="trainer picture" onerror="this.src=\'/images/user-3.svg\'" height="40"/>' +
                            '<span style="margin-top: -10px" class="autocomplete-li-with-picture">' + item.name +
                            '<span class="autocomplete-gymname">' + " (" + item.gymName + ")" + '</span></span></a>',
                            value: item.name
                        }
                    }));
                }
            });
        },
        minLength: 1,
        html: true,
    });
});