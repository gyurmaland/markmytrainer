$(function () {
    var rating = 0;

    $(".rateYoReadOnly").each(function (index, element) {

        if($(this).parent().children(".starOutput").text() == '') {
            rating = 0;
        } else {
            rating = $(this).parent().children(".starOutput").text();
        }

        $(this).rateYo({
            rating: rating,
            numStars: 5,
            precision: 2,
            halfStar: true,
            readOnly: true,
            starWidth: "24px",
            spacing: "6px",
            onInit: function () {
                console.log("On Init");
            },
            onSet: function () {
                console.log("On Set");
            }
        });
    });
});