$(function () {
    var rating = 0;

    $(".rateYoReadOnly.notblur").each(function (index, element) {
        $(this).rateYo({
            rating: $(this).parent().children(".starOutput").text(),
            numStars: 5,
            precision: 2,
            halfStar: true,
            readOnly: true,
            starWidth: "24px",
            spacing: "6px",
            onInit: function () {
                console.log("On Init");
            },
            onSet: function () {
                console.log("On Set");
            }
        });
    });
    $(".rateYoReadOnly.blur").each(function (index, element) {
        $(this).rateYo({
            rating: 0,
            numStars: 5,
            precision: 2,
            halfStar: true,
            readOnly: true,
            starWidth: "24px",
            spacing: "6px",
            onInit: function () {
                console.log("On Init");
            },
            onSet: function () {
                console.log("On Set");
            }
        });
    });

});