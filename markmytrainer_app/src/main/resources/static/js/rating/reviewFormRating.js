$(function () {

    var rating = document.getElementById("starInput").value;

    $(".counter").text(rating);

    $("#rateYo1").on("rateyo.init", function () {
        console.log("rateyo.init");
    });

    $("#rateYo1").rateYo({
        rating: rating,
        numStars: 5,
        precision: 2,
        fullStar: true,
        starWidth: "34px",
        spacing: "6px",
        onInit: function () {

            console.log("On Init");
        },
        onSet: function () {
            console.log("On Set");
        }
    }).on("rateyo.set", function () {
        var currentRating = $("#rateYo1").rateYo("rating");
        var ratingInput = document.getElementById("starInput");
        ratingInput.value = currentRating;
        console.log("rateyo.set");
    }).on("rateyo.change", function () {
        console.log("rateyo.change");
    });
});