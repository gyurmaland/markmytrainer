function dataURItoBlob(dataURI) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);

    // create a view into the buffer
    var ia = new Uint8Array(ab);

    // set the bytes of the buffer to the correct values
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    var blob = new Blob([ab], {type: mimeString});
    return blob;

}


$(document).ready(function(){

    // preparing canvas variables
    var $canvas = $('#canvas'),
        context = $canvas.get(0).getContext('2d');

    // waiting for a file to be selected
    $('#fileUpload').on('change',function(){

        if (this.files && this.files[0]) {
            // checking if a file is selected

            if ( this.files[0].type.match(/^image\//) ) {
                // valid image file is selected
                // process image
                // process the image
                var reader = new FileReader();

                reader.onload = function(e){

                    $canvas.cropper('destroy');


                    var img = new Image();
                    img.onload = function() {
                        context.canvas.width = img.width;
                        context.canvas.height = img.height;
                        context.drawImage(img, 0, 0);

                        // instantiate cropper
                        var cropper = $canvas.cropper({
                            aspectRatio: 1 / 1,
                            dragMode: 'move',
                            cropBoxMovable: false,
                            cropBoxResizable: false,
                            minCropBoxWidth: 128,
                            minCropBoxHeight: 128,
                        });
                    };
                    img.src = e.target.result;
                };

                $('#crop').click(function(){
                    var originalCropeedImage = $canvas.cropper('getCroppedCanvas');
                    var croppedImage = $canvas.cropper('getCroppedCanvas').toDataURL('image/jpg');
                    $('#result').append($('<img>').attr('src', croppedImage));
                    console.log(croppedImage);

                    var blob = dataURItoBlob(croppedImage, 'image/jpg');

                    var form = $('#trainerForm')[0];

                    var data = new FormData(form);

                    data.append("file", blob);

                    console.log(data);


                    $.ajax({
                        type: "POST",
                        url: '/gyms/addTrainer', // point to server-side controller method
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: data,
                        enctype: "multipart/form-data",
                        success: function (response) {
                            $('#result').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('#result').html(response); // display error response from the server
                        }
                    });



                });

                // reading the selected file
                reader.readAsDataURL(this.files[0]);


            } else {
                alert('Invalid file type');
            }
        } else {
            alert('Please select a file.');
        }
    });

});










/*

$(document).ready(function () {

    $("#btnSubmit").click(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });

});

function fire_ajax_submit() {

    // Get form
    var form = $('#trainerForm')[0];

    var data = new FormData(form);

    data.append("CustomField", "This is some extra data, testing");

    $("#btnSubmit").prop("disabled", true);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "/api/upload",
        data: data,
        //http://api.jquery.com/jQuery.ajax/
        //https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects
        processData: false, //prevent jQuery from automatically transforming the data into a query string
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {

            $("#result").text(data);
            console.log("SUCCESS : ", data);
            $("#btnSubmit").prop("disabled", false);

        },
        error: function (e) {

            $("#result").text(e.responseText);
            console.log("ERROR : ", e);
            $("#btnSubmit").prop("disabled", false);

        }
    });

}*/
