$(".chzn-select").chosen({
    disable_search: true,
    disable_search_threshold: 10,
    no_results_text: "No gym found",
    width: "100%"
});


$('#inputTrainerAddTrainerCityName').on("change keyup paste click touchstart", function () {
    $.getJSON("gymsByCityName", {
        cityName: $(this).val(),
        ajax: 'true',
    }, function (data) {
        if (data.length == 0) {
            console.log('There are no gyms in ' + $('#inputTrainerAddTrainerCityName').val());
            if ($('#inputTrainerAddTrainerCityName').val().length > 0) {
                $('#gymNotFoundInCityErrorMsg').show();
                $('#gymNotFoundInCityErrorMsg').html('Itt még nincs edzőterem: ' + $('#inputTrainerAddTrainerCityName').val() +
                    '</br>' +  'katt <a href="/gyms/add">IDE</a> és adj hozzá!');
            }
        } else {
            console.log('YASSSS');
            $('#gymNotFoundInCityErrorMsg').hide();
            $('#inputTrainerAddTrainerGymName-error').hide();
        }
        var html = '';
        $.each(data, function (key, val) {
            html += '<option value="' + val.id + '">'
                + val.name + '</option>';
        });


        $('.chzn-select').html(html);
        $('.chzn-select').trigger("chosen:updated");
    });
});