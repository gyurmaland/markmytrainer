jQuery(document).ready(function ($) {

    // Remove empty fields from GET forms
    // Author: Bill Erickson
    // URL: http://www.billerickson.net/code/hide-empty-fields-get-form/

    // Change 'form' to class or ID of your specific form
    $("#searchTrainerForm").submit(function () {
        $(this).find(":input").filter(function () {
            return !this.value;
        }).attr("disabled", "disabled");
        return true; // ensure form still submits
    });

    // Un-disable form fields when page loads, in case they click back after submission
    $("#searchTrainerForm").find(":input").prop("disabled", false);

});

jQuery(document).ready(function ($) {

    if( $( "#searchTrainerName" ).val() != "") {
        $( "#reset-fields" ).append( "<a href='/trainers' class='text-danger'>Szűrés törlése</a>" );
    }

    if ($( "#searchGymName" ).val() != "" || $( "#searchCityName" ).val() != "") {
        $( "#trainerSearchGymNameDiv" ).show();
        $( "#trainerSearchCityNameDiv" ).show();

        $( "#reset-fields" ).append( "<a href='/trainers' class='text-danger'>Szűrés törlése</a>" );
    }


    $( "#dropdownMenuButton" ).click(function() {
        $( "#trainerSearchGymNameDiv" ).toggle();
        $( "#trainerSearchCityNameDiv" ).toggle();
        $( ".adv-search-toggle" ).toggleClass("rotated-adv-search-arrow");
    });
});

$(document).ready(function (){
    validate();
    $('#searchTrainerName, #searchGymName, #searchCityName').on("change keyup paste click touchstart", validate);
});

function validate(){
    if ($('#searchTrainerName').val().length   >   0   ||
        $('#searchGymName').val().length  >   0 ||
        $('#searchCityName').val().length  >   0) {
        $("#trainerSearchButton").prop("disabled", false);
    }
    else {
        $("#trainerSearchButton").prop("disabled", true);
    }
}