jQuery(document).ready(function ($) {

    if( $( "#searchTrainerName" ).val() != "") {
        $( "#reset-fields" ).append( "<a href='/gyms/" + gymId +"/' class='text-danger'>Szűrés törlése</a>" );
    }
});

$(document).ready(function (){
    validate();
    $('#searchTrainerName').on("change keyup paste click touchstart", validate);
});

function validate(){
    if ($('#searchTrainerName').val().length   >   0) {
        $("#trainerSearchButton").prop("disabled", false);
    }
    else {
        $("#trainerSearchButton").prop("disabled", true);
    }
}