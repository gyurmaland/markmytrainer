jQuery(document).ready(function ($) {

    // Remove empty fields from GET forms
    // Author: Bill Erickson
    // URL: http://www.billerickson.net/code/hide-empty-fields-get-form/

    // Change 'form' to class or ID of your specific form
    $("#searchGymForm").submit(function () {
        $(this).find(":input").filter(function () {
            return !this.value;
        }).attr("disabled", "disabled");
        return true; // ensure form still submits
    });

    // Un-disable form fields when page loads, in case they click back after submission
    $("#searchGymForm").find(":input").prop("disabled", false);

});

jQuery(document).ready(function ($) {

    if( $( "#searchGymName" ).val() != "" || $( "#searchCityName" ).val() != "") {
        $( "#reset-fields" ).append( "<a href='/gyms' class='text-danger'>Szűrés törlése</a>" );
    }

    if( $( "#searchCityName" ).val() != "") {
        $( "#gymSearchCityNameDiv" ).show();
        if (!$("#reset-fields").length) {
            $( "#reset-fields" ).append( "<a href='/gyms' class='text-danger'>Szűrés törlése</a>" );
        }
    }

    $( "#dropdownMenuButton" ).click(function() {
        $( "#gymSearchCityNameDiv" ).toggle();
        $( ".adv-search-toggle" ).toggleClass("rotated-adv-search-arrow");
    });
});


$(document).ready(function (){
    validate();
    $('#searchGymName, #searchCityName').on("change keyup paste click touchstart", validate);
});

function validate(){
    if ($('#searchGymName').val().length   >   0   ||
        $('#searchCityName').val().length  >   0) {
        $("#gymSearchButton").prop("disabled", false);
    }
    else {
        $("#gymSearchButton").prop("disabled", true);
    }
}



