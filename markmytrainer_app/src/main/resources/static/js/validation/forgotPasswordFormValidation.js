$(document).ready(function(){
    $.validator.setDefaults({
        errorClass: 'invalid-feedback',
        highlight: function (element) {
            $(element)
                .closest('input')
                .addClass('has-error');
        },
        unhighlight: function (element) {
            $(element)
                .closest('input')
                .removeClass('has-error');
        }
    });

    $("#forgotPasswordForm").validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: 'Adj meg egy e-mailt lécc!',
                email: 'Valós e-mailt adj meg!'
            }
        }
    });
});