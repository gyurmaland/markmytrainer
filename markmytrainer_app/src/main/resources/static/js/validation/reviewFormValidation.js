$(document).ready(function(){
    $.validator.setDefaults({
        ignore: ":hidden:not(input)",
        errorClass: 'invalid-feedback',
        highlight: function (element) {
            $(element)
                .closest('input')
                .addClass('has-error');
        },
        unhighlight: function (element) {
            $(element)
                .closest('input')
                .removeClass('has-error');
        }
    });

    $.validator.addMethod('ratingValid', function (value, element) {
        return value >= 1.0 && value <= 5.0;
    }, "A pontszám legyen 1 és 5 között!");

    $.validator.addMethod('ratingTextLength', function (value, element) {
        return value.length >= 3 && value.length <=1000;
    }, "Az értékelő szöveg 3 és 1000 karakter között legyen légyszí!");

    $("#reviewForm").validate({
        rules: {
            rating: {
                required: true,
                range: [1, 5]
            },
            reviewText: {
                required: true,
                ratingTextLength: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "rating") {
                error.insertAfter("#rateYo1");
            }else{
                error.insertAfter(element);
            }
        },
        messages: {
            rating: {
                required: 'Adj meg egy pontszámot!',
                range: 'A pontszám legyen 1 és 5 között!'
            },
            reviewText: {
                required: 'Valami értékelést írj ide!!!'
            }
        }
    });
});