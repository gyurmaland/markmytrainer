$(document).ready(function(){
    $.validator.setDefaults({
        errorClass: 'invalid-feedback',
        highlight: function (element) {
            $(element)
                .closest('input')
                .addClass('has-error');
        },
        unhighlight: function (element) {
            $(element)
                .closest('input')
                .removeClass('has-error');
        }
    });

    $.validator.addMethod('usernameLength', function (value, element) {
        return value.length >= 3 && value.length <=20;
    }, "A felhasználónevednek 3 és 20 karakter közöttinek kell lennie!");

    $.validator.addMethod('cityNameLength', function (value, element) {
        return value.length >= 2 && value.length <=100;
    }, "A város nevének 2 és 100 közöttinek kell lennie, vagy nem? :D ");

    $.validator.addMethod('strongPassword', function (value, element) {
        return value.length >= 6;
    }, "A jelszáú minimum 6 krakter legyen!");

    $("#signUpForm").validate({
        rules: {
            username: {
                required: true,
                usernameLength: true
            },
            email: {
                required: true,
                email: true
            },
            cityName: {
                required: true,
                cityNameLength: true
            },
            password: {
                required: true,
                strongPassword: true
            },
            confirmPassword: {
                required: true,
                equalTo: "#inputSignUpPassword"
            }
        },
        messages: {
            username: {
                required: 'Egy felhasználónév azért kéne, nem gondolod?'
            },
            email: {
                required: 'Adj meg egy e-mail címet!',
                email: 'Valós e-mail címet adj meg!'
            },
            cityName: {
                required: 'Add meg a jelnlegi városodat!',
            },
            password: {
                required: 'Adj meg egy jelszót azért...!'
            },
            confirmPassword: {
                required: 'Biztos, ami biztos üsd be megint!',
                equalTo: 'Ugyanazt a jelszót add meg, mint amit az előbb. Nehéz?'
            },
        }
    });
});