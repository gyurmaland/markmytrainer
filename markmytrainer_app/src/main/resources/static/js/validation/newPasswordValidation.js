$(document).ready(function(){
    $.validator.setDefaults({
        errorClass: 'invalid-feedback',
        highlight: function (element) {
            $(element)
                .closest('input')
                .addClass('has-error');
        },
        unhighlight: function (element) {
            $(element)
                .closest('input')
                .removeClass('has-error');
        }
    });

    $.validator.addMethod('newPasswordLength', function (value, element) {
        return value.length >= 6;
    }, "Fő a biztonság, a jelszavad minimum 6 karakter legyen!");

    $("#passwordChangeForm").validate({
        rules: {
            oldPassword: {
                required: true
            },
            newPassword: {
                required: true,
                newPasswordLength: true
            },
            confirmNewPassword: {
                required: true,
                equalTo: "#inputNewPassword"
            }
        },
        messages: {
            oldPassword: {
                required: 'Add meg a régi jelszavad!'
            },
            newPassword: {
                required: 'Add meg az új jelszavad!',
            },
            confirmNewPassword: {
                required: 'Add meg újra az új jelszavad, hehe :D',
                equalTo: 'Ugyanazt add meg, mint felül!'
            }
        }
    });
});