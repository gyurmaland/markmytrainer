$(document).ready(function(){
    $.validator.setDefaults({
        errorClass: 'invalid-feedback',
        highlight: function (element) {
            $(element)
                .closest('input')
                .addClass('has-error');
        },
        unhighlight: function (element) {
            $(element)
                .closest('input')
                .removeClass('has-error');
        }
    });

    $.validator.addMethod('newPasswordLength', function (value, element) {
        return value.length >= 6;
    }, "A jelszónak legalább 6 karakternek kell lennie, mert fő a biztonság! :)");

    $("#passwordResetForm").validate({
        rules: {
            password: {
                required: true,
                newPasswordLength: true
            },
            confirmPassword: {
                required: true,
                equalTo: "#inputPasswordReset"
            }
        },
        messages: {
            password: {
                required: 'Add meg az új jelszavad!!'
            },
            confirmPassword: {
                required: 'Add meg megint az új jelszavad!',
                equalTo: 'Ugyanazt add meg, mint felül!'
            }
        }
    });
});