$(document).ready(function(){
    $.validator.setDefaults({
        errorClass: 'invalid-feedback',
        highlight: function (element) {
            $(element)
                .closest('input')
                .addClass('has-error');
        },
        unhighlight: function (element) {
            $(element)
                .closest('input')
                .removeClass('has-error');
        }
    });

    $("#trainerForm").validate({
        rules: {
            name: {
                required: true,
                trainerNameLength: true
            }
        },
        messages: {
            name: {
                required: 'Adj meg egy nevet'
            }
        }
    });
});