$(document).ready(function(){
    $.validator.setDefaults({
        errorClass: 'invalid-feedback',
        highlight: function (element) {
            $(element)
                .closest('input')
                .addClass('has-error');
        },
        unhighlight: function (element) {
            $(element)
                .closest('input')
                .removeClass('has-error');
        }
    });

    $.validator.addMethod('cityNameLength', function (value, element) {
        return value.length >= 2 && value.length <=20;
    }, "A város neve 2 és 20 karakter között lehet.");

    $("#cityEditForm").validate({
        rules: {
            cityName: {
                required: true,
                cityNameLength: true
            }
        },
        messages: {
            cityName: {
                required: 'Kérlek adj meg egy várost!'
            }
        }
    });
});