$(document).ready(function(){
    $.validator.setDefaults({
        errorClass: 'invalid-feedback',
        highlight: function (element) {
            $(element)
                .closest('input')
                .addClass('has-error');
        },
        unhighlight: function (element) {
            $(element)
                .closest('input')
                .removeClass('has-error');
        }
    });

    $.validator.addMethod('cityNameLength', function (value, element) {
        return value.length >= 2 && value.length <=20;
    }, "A város nevének 2 és 20 karakter között kell lennie");

    $.validator.addMethod('gymNameLength', function (value, element) {
        return value.length >= 3 && value.length <=100;
    }, "A terem nevének 3 és 100 karkter között kell lennie.");

    $("#gymForm").validate({
        rules: {
            cityName: {
                required: true,
                cityNameLength: true
            },
            name: {
                required: true,
                gymNameLength: true
            }
        },
        messages: {
            cityName: {
                required: 'Adj meg egy várost!'
            },
            name: {
                required: 'Adj meg egy termet!'
            }
        }
    });
});