$(document).ready(function(){
    $.validator.setDefaults({
        errorClass: 'invalid-feedback',
        highlight: function (element) {
            $(element)
                .closest('input, textarea')
                .addClass('has-error');
        },
        unhighlight: function (element) {
            $(element)
                .closest('input')
                .removeClass('has-error');
        }
    });

    $.validator.addMethod('nameLength', function (value, element) {
        return value.length >= 2 && value.length <=100;
    }, "A név 2 és 100 karakter között legyen.");

    $.validator.addMethod('messageLength', function (value, element) {
        return value.length >= 5;
    }, "Legalább 5 karakter üzit írjá' be.");

    $("#contactForm").validate({
        rules: {
            name: {
                required: true,
                nameLength: true
            },
            email: {
                required: true,
                email: true
            },
            message: {
                required: true,
                messageLength: true
            }
        },
        messages: {
            name: {
                required: 'Kérlek adj meg egy nevet (bármi lehet)!'
            },
            email: {
                required: 'írj be egy email címet!!',
                email: 'Valós e-mail címet írj be!'
            },
            message: {
                required: 'Valami üzenetet írj be!',
            }
        }
    });
});