$(document).ready(function(){
    $.validator.setDefaults({
        ignore: ":hidden:not(select)",
        errorClass: 'invalid-feedback',
        highlight: function (element) {
            $(element)
                .closest('input')
                .addClass('has-error');
        },
        unhighlight: function (element) {
            $(element)
                .closest('input')
                .removeClass('has-error');
        }
    });

    $.validator.addMethod('trainerNameLength', function (value, element) {
        return value.length >= 3 && value.length <=100;
    }, "Az edző neve 3 és 100 karakter legyen!");

    $.validator.addMethod('cityNameLength', function (value, element) {
        return value.length >= 2 && value.length <=100;
    }, "A város neve 2 és 100 karakter között legyen!");

    $("#trainerForm").validate({
        rules: {
            name: {
                required: true,
                trainerNameLength: true
            },
            cityName: {
                required: true,
                cityNameLength: true
            },
            'gym.id': {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "gym.id") {
                error.insertAfter("#inputTrainerAddTrainerGymName_chosen");
            }else{
                error.insertAfter(element);
            }
        },
        messages: {
            name: {
                required: 'Adj meg egy nevet!'
            },
            cityName: {
                required: 'Add meg a város nevét, ahol az edző dolgozik!'
            },
            'gym.id': {
                required: 'Válassz egy termet!'
            }
        }
    });
});