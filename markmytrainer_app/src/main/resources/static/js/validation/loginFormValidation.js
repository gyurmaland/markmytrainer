$(document).ready(function(){
    $.validator.setDefaults({
        errorClass: 'invalid-feedback',
        highlight: function (element) {
            $(element)
                .closest('input')
                .addClass('has-error');
        },
        unhighlight: function (element) {
            $(element)
                .closest('input')
                .removeClass('has-error');
        }
    });

    $("#loginForm").validate({
        rules: {
            username: {
                required: true
            },
            password: {
                required: true
            }
        },
        messages: {
            username: {
                required: 'Felhasználónév nélkül nehéz belépni...'
            },
            password: {
                required: 'Jelszó nélkül nehéz belépni...'
            }
        }
    });
});