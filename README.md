# markmytrainer

A markmytrainer egy olyan web applikáció, ahol különböző konditermekben tevékenykedő személyi edzőkről lehet értékeléseket/információkat olvasni, illetve hozzáadni.
Ha az oldalon nem található az adott edző, hozzá lehet őt adni. 
A felhasználók meg tudják keresni Magyarországon belül az adott város adott konditeremében tevékenykedő személyi edzőket és névtelen értékeléseket tudnak róluk olvasni/írni.

A program később skálázható olyan applikációvá, ahol nemcsak értékeléseket lehetne olvasni, hanem akár fel is lehet venni a kapcsolatot az adott edzővel.


## Built With

* [Spring](https://spring.io/)
* [Gradle](https://gradle.org/)
* [Thymeleaf](http://www.thymeleaf.org/)
* [Hibernate](http://hibernate.org/)
* [Spring Security](https://projects.spring.io/spring-security/)


